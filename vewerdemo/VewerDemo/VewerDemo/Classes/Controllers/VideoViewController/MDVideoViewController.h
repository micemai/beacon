//
//  MDVideoViewController.h
//  MutimediaDemo
//
//  Created by Nguyen Phuong Mai on 9/24/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface MDVideoViewController : UIViewController
@property (nonatomic, strong) NSURL *URL;
- (IBAction)loadMovieButtonPressed:(id)sender;

@end
