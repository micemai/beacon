//
//  VDBeaconViewController.m
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/26/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import "VDBeaconViewController.h"
#import "MDVideoViewController.h"
#import "VDWebViewController.h"
#import "VDAppDelegate.h"
#import <MapKit/MapKit.h>
#import "VDBeanconInfo.h"
#import "VDAction.h"
#define MERCATOR_OFFSET 268435456
#define MERCATOR_RADIUS 85445659.44705395
@interface VDBeaconViewController ()<UITableViewDataSource,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblView;


@end

@implementation VDBeaconViewController
+ (instancetype)viewControllerFromNib
{
    NSString *nibName = NSStringFromClass(self);
    NSArray *xibs = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id xib in xibs) {
        if ([xib isKindOfClass:self]) {
            return xib;
        }
    }
    return nil;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.arrBeaconInfo = [[NSMutableArray alloc] init];
    }
    return self;
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
[center addObserver:self selector:@selector(didReloadTable:) name:VDConstants_ReloadTableNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
	NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
	[center removeObserver:self];
    
	[super viewWillDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) stringIsNumeric:(NSString *) str {
    NSRange baseRange = NSMakeRange(0, str.length);
    NSRange range = [str rangeOfString:@"[0-9]+" options:NSRegularExpressionSearch range:baseRange];
    BOOL isNumberOnly = NSEqualRanges(baseRange, range);
    return isNumberOnly;
    
}
#pragma mark - Private
- (void) didReloadTable:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tblView reloadData];
    });
}

- (void) didOpenWebView:(VDAction *)action
{
	NSString *strURL = action.param2;
    VDWebViewController *webViewController = [[VDWebViewController alloc] initWithNibName:@"VDWebViewController" bundle:nil];
    webViewController.URL = [NSURL URLWithString:strURL];
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void) didPlayMovie:(VDAction *)action
{
	NSString *strURL = action.param2;
    MDVideoViewController *videoViewController = [[MDVideoViewController alloc] initWithNibName:@"MDVideoViewController" bundle:nil];
    videoViewController.URL = [NSURL URLWithString:strURL];
    [self.navigationController pushViewController:videoViewController animated:YES];
}
- (void) didOpenYoutubeVideo:(VDAction *)action
{
	NSString *strURL = [NSString stringWithFormat:@"%@?autoplay=1",action.param2];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strURL]];
    
}
- (void) didOpenMap:(VDAction *)action
{
	NSString *strOrigin = action.param2;
    NSUInteger zoomLevel = [VDConstants_MapRoomLevelDefault integerValue];
    if (action.param3 && [self stringIsNumeric:action.param3]) {
        zoomLevel = [action.param3 integerValue];//39321
    }
    
    NSArray *subStrings = [strOrigin componentsSeparatedByString:@","]; //or rather @" - "
    if ([subStrings count] == 2) {
        NSString *strLatitude = [subStrings objectAtIndex:0];
        NSString *strLongitude = [subStrings objectAtIndex:1];
        
        CLLocationCoordinate2D coords =
        CLLocationCoordinate2DMake([strLatitude doubleValue], [strLongitude doubleValue]);
        
        //    NSDictionary *address = @{
        //
        //                              };
        //MKMapRect zoomRect = MKMapRectMake(40.74835, -73.984911, [strLevel boolValue], [strLevel boolValue]);
        
        MKPlacemark *place = [[MKPlacemark alloc]
                              initWithCoordinate:coords addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc]
                              initWithPlacemark:place];
        MKCoordinateSpan span = [self coordinateSpanWithCenterCoordinate:coords andZoomLevel:zoomLevel];
        NSDictionary *options = @{MKLaunchOptionsMapSpanKey:[NSValue valueWithMKCoordinateSpan:span]};
        [mapItem openInMapsWithLaunchOptions:options];
    }
   
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@&z=%@",strOrigin,strLevel]]];
}
- (double)longitudeToPixelSpaceX:(double)longitude
{
    return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * M_PI / 180.0);
}

- (double)latitudeToPixelSpaceY:(double)latitude
{
    return round(MERCATOR_OFFSET - MERCATOR_RADIUS * logf((1 + sinf(latitude * M_PI / 180.0)) / (1 - sinf(latitude * M_PI / 180.0))) / 2.0);
}

- (double)pixelSpaceXToLongitude:(double)pixelX
{
    return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / M_PI;
}

- (double)pixelSpaceYToLatitude:(double)pixelY
{
    return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / M_PI;
}


- (MKCoordinateSpan)coordinateSpanWithCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                 andZoomLevel:(NSUInteger)zoomLevel
{
    // convert center coordiate to pixel space
    double centerPixelX = [self longitudeToPixelSpaceX:centerCoordinate.longitude];
    double centerPixelY = [self latitudeToPixelSpaceY:centerCoordinate.latitude];
    
    // determine the scale value from the zoom level
    NSInteger zoomExponent = 20 - zoomLevel;
    double zoomScale = pow(2, zoomExponent);
    
    // scale the map’s size in pixel space
    CGSize mapSizeInPixels = self.view.bounds.size;
    double scaledMapWidth = mapSizeInPixels.width * zoomScale;
    double scaledMapHeight = mapSizeInPixels.height * zoomScale;
    
    // figure out the position of the top-left pixel
    double topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
    double topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
    
    // find delta between left and right longitudes
    CLLocationDegrees minLng = [self pixelSpaceXToLongitude:topLeftPixelX];
    CLLocationDegrees maxLng = [self pixelSpaceXToLongitude:topLeftPixelX + scaledMapWidth];
    CLLocationDegrees longitudeDelta = maxLng - minLng;
    
    // find delta between top and bottom latitudes
    CLLocationDegrees minLat = [self pixelSpaceYToLatitude:topLeftPixelY];
    CLLocationDegrees maxLat = [self pixelSpaceYToLatitude:topLeftPixelY + scaledMapHeight];
    CLLocationDegrees latitudeDelta = -1 * (maxLat - minLat);
    
    // create and return the lat/lng span
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    return span;
}
#pragma mark - UITableViewDatasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.arrBeaconInfo.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"VDBeaconInfoCell";
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    VDBeanconInfo *beaconInfo = (VDBeanconInfo*)[self.arrBeaconInfo objectAtIndex:indexPath.row];
	NSString *strCellLabel = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@",beaconInfo.strUUID,beaconInfo.strMajor,beaconInfo.strMinor,beaconInfo.strBID,beaconInfo.strLocation,beaconInfo.strPlacement,beaconInfo.strAction,beaconInfo.strRSSI,beaconInfo.strDateTime];
    //cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    cell.textLabel.numberOfLines = 9;
    cell.textLabel.font = [UIFont systemFontOfSize:10];
    cell.textLabel.text = strCellLabel;
	
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	VDBeanconInfo *beaconInfo = (VDBeanconInfo*)[self.arrBeaconInfo objectAtIndex:indexPath.row];
    NSString *param1 = (NSString *)beaconInfo.paramsAction[0];
	NSString *param2 = (NSString *)beaconInfo.paramsAction[1];
	NSString *param3 = (NSString *)beaconInfo.paramsAction[2];
	NSString *param4 = (NSString *)beaconInfo.paramsAction[3];
	NSString *param5 = (NSString *)beaconInfo.paramsAction[4];
    VDAction *action = [[VDAction alloc] init];
	action.action = param1;
	action.param1 = param2;
	action.param2 = param3;
	action.param3 = param4;
	action.param4 = param5;
    if (![self isValidAction:action]) {
		NSLog(@"アクションが不正です。'%@'", action);
	}
    if ([action.action isEqualToString:[self htmlAction]])
    {
        [self didOpenWebView:action];
    }
    else if ([action.action isEqualToString:[self movieAction]])
    {
        [self didPlayMovie:action];
    }
    else if ([action.action isEqualToString:[self youtubeAction]])
    {
        [self didOpenYoutubeVideo:action];
    }
    else if ([action.action isEqualToString:[self mapAction]])
    {
        [self didOpenMap:action];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (BOOL)isValidAction:(VDAction *)action
{
	if ([[self validActions] containsObject:action.action]) {
		NSLog(@"action は妥当です。 action='%@'", action.action);
	} else {
		NSLog(@"action は不正です。 action='%@'", action.action);
		return NO;
	}
    return YES;
}
- (NSArray *)validActions
{
    return @[[self htmlAction], [self movieAction], [self youtubeAction], [self mapAction]];
}

- (NSString *)htmlAction
{
	return @"html";
}

- (NSString *)movieAction
{
	return @"movie";
}

- (NSString *)youtubeAction
{
	return @"youtube";
}
- (NSString *)mapAction
{
	return @"map";
}
@end
