//
//  VDBeaconViewController.h
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/26/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VDBeaconViewController : UIViewController
@property (nonatomic, strong) NSMutableArray* arrBeaconInfo;
+ (instancetype)viewControllerFromNib;

@end
