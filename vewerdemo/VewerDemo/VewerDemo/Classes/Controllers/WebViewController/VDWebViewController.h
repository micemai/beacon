//
//  VDWebViewController.h
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/26/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VDWebViewController : UIViewController
@property (nonatomic, strong) NSURL *URL;
+ (instancetype)viewControllerFromNib;
@end
