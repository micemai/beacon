//
//  VDWebViewController.m
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/26/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import "VDWebViewController.h"

@interface VDWebViewController ()
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIButton *closeButton;
@end
@interface VDWebViewController (Action)

- (IBAction)didCloseButtonTapped:(id)sender;

@end
@interface VDWebViewController (WebView) <UIWebViewDelegate>

@end
@implementation VDWebViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"Close" forState:UIControlStateNormal];
    
    [button sizeToFit];
    [button addTarget:self action:@selector(didCloseButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    [[self navigationItem] setRightBarButtonItem:item];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    
	NSURLRequest *request = [NSURLRequest requestWithURL:self.URL];
	[self.webView loadRequest:request];
}
+ (instancetype)viewControllerFromNib
{
    NSString *nibName = NSStringFromClass(self);
    NSArray *xibs = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id xib in xibs) {
        if ([xib isKindOfClass:self]) {
            return xib;
        }
    }
    return nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0.408 green:0.761 blue:0.914 alpha:1];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
////////////////////////////////////////////////////////////////////////////////


@implementation VDWebViewController (Action)

- (void)didCloseButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

////////////////////////////////////////////////////////////////////////////////

@implementation VDWebViewController (WebView)

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"window.document.title"];
    [self setTitle:title];
}

@end
