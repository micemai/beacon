//
//  VDConstants.m
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/25/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.

#import "VDConstants.h"

#ifdef DEBUG

// BeaconnectSDKのTimeout
NSTimeInterval VDConstants_BeaconTimeoutInterval = 30.0f;
// BeaconnectのServiceID
NSString * const VDConstants_SID = @"999996";
// BeaconnectのAPIKey
NSString * const VDConstants_APIKey = @"55BA0E34-890D-47C1-B358-BA7BC2A1DCA0";
// BeaconnectのBaseURL
NSString * const VDConstants_BeaconnectBaseURL = @"http://beacon.myu-on.net/";


#else
// BeaconnectSDKのTimeout
NSTimeInterval VDConstants_BeaconTimeoutInterval = 30.0f;
// BeaconnectのServiceID
NSString * const VDConstants_SID = @"999996";
// BeaconnectのAPIKey
NSString * const VDConstants_APIKey = @"55BA0E34-890D-47C1-B358-BA7BC2A1DCA0";
// BeaconnectのBaseURL
NSString * const VDConstants_BeaconnectBaseURL = @"http://beacon.myu-on.net/";


#endif

NSString * const VDConstants_LocalNotificationNameKey = @"VDConstants_LocalNotificationNameKey";
NSString * const VDConstants_LocalNotificationUserInfoKey = @"VDConstants_LocalNotificationUserInfoKey";


NSString * const VDConstants_ReloadTableNotification = @"VDConstants_ReloadTableNotification";
NSString * const VDConstants_MapRoomLevelDefault = @"10";
@implementation VDConstants
@end