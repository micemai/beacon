//
//  VDConstants.h
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/25/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.

extern NSTimeInterval VDConstants_BeaconTimeoutInterval;
extern NSString * const VDConstants_SID;
extern NSString * const VDConstants_APIKey;
extern NSString * const VDConstants_BeaconnectBaseURL;
extern NSString * const VDConstants_ContentServerBaseURL;

extern NSString * const VDConstants_LocalNotificationNameKey;
extern NSString * const VDConstants_LocalNotificationUserInfoKey;

extern NSString * const VDConstants_ReloadTableNotification;

extern NSString * const VDConstants_MapRoomLevelDefault;

static void *MyStreamingMovieViewControllerTimedMetadataObserverContext = &MyStreamingMovieViewControllerTimedMetadataObserverContext;
static void *MyStreamingMovieViewControllerRateObservationContext = &MyStreamingMovieViewControllerRateObservationContext;
static void *MyStreamingMovieViewControllerCurrentItemObservationContext = &MyStreamingMovieViewControllerCurrentItemObservationContext;
static void *MyStreamingMovieViewControllerPlayerItemStatusObserverContext = &MyStreamingMovieViewControllerPlayerItemStatusObserverContext;
@interface VDConstants : NSObject

@end
