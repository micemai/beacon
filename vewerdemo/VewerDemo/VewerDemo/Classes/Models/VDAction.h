//
//  VDAction.h
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/26/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VDAction : NSObject
@property (nonatomic, strong) NSString *action;
@property (nonatomic, strong) NSString *param1;
@property (nonatomic, strong) NSString *param2;
@property (nonatomic, strong) NSString *param3;
@property (nonatomic, strong) NSString *param4;

@end
