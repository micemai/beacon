//
//  VDBeancon.h
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/29/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VDBeanconInfo : NSObject
@property (nonatomic, strong) NSString *strUUID;
@property (nonatomic, strong) NSString *strMajor;
@property (nonatomic, strong) NSString *strMinor;
@property (nonatomic, strong) NSString *strBID;
@property (nonatomic, strong) NSString *strLocation;
@property (nonatomic, strong) NSString *strPlacement;
@property (nonatomic, strong) NSString *strAction;
@property (nonatomic, strong) NSString *strRSSI;
@property (nonatomic, strong) NSString *strDateTime;
@property (nonatomic, strong) NSArray *paramsAction;
@end
