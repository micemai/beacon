//
//  BIMobliePhone.h
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/24.
//
//

#import <Foundation/Foundation.h>

@interface BIMobilePhone : NSObject

@property (assign, nonatomic) NSInteger seqId;
@property (assign, nonatomic) NSInteger serviceId;
@property (strong, nonatomic) NSString *mpid;
@property (strong, nonatomic) NSString *osType;
@property (strong, nonatomic) NSString *pushToken;

@end
