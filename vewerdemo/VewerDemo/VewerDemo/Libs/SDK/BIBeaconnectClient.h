//
//  BIBeaconnectClient.h
//  BeaconConnect
//
//  Created by zhu on 2014/07/17.
//  Copyright (c) 2014年 zhu. All rights reserved.
//  Version: 1.0

#import <Foundation/Foundation.h>
#import "BIBeacon.h"
#import "BIBeaconData.h"
#import "BIBeaconDetail.h"
#import "BIDriversManager.h"
#import "BIBeaconLog.h"


@class BIBeaconnectClient;

/*!
    @protocol BIBeaconnectScanningDelegate
 
    @discussion
 */
@protocol BIBeaconnectScanningDelegate <NSObject>

/*!
    検知ビーコンリストに新たに検知したビーコンが追加されたとき
  * 
  * @param beacon 検知したビーコン
  */
- (void)client:(BIBeaconnectClient *)client didAddDetectList:(BIBeacon *) beacon;

/**
 * もっとも近くにあるビーコンが別のビーコンに変わったとき
 *
 * @param beacon 近くにあるビーコン
 */
- (void)client:(BIBeaconnectClient *)client didUpdateNearest:(BIBeacon *) beacon;

/**
 * ビーコン範囲から出たとき（一定時間検知していないビーコンがあった場合）
 *
 * @param beacon 範囲から出たビーコン
 */
- (void)client:(BIBeaconnectClient *)client didOutOfRange:(BIBeacon *) beacon;


@end

/**
  *
  */
@interface BIBeaconnectClient : NSObject

/*!
 */
@property (strong, nonatomic) NSString *serverBaseURL;

/**
 * ビーコン検知開始
 *
 * @param serviceId サービスコード
 * @param apiKey APIキー
 * @param delegate
 */
-(id)initWithServiceId:(NSString *)sid apiKey:(NSString *)apiKey timeout:(NSTimeInterval)timeout delegate:(id <BIBeaconnectScanningDelegate>) delegate;


/**
 * ビーコン検知開始
 *
 * @param serviceId サービスコード
 * @param apiKey APIキー
 * @param delegate
 */
- (id)initWithBaseURL:(NSURL *)baseURL serviceId:(NSString *)sid apiKey:(NSString *)apiKey timeout:(NSTimeInterval)timeout delegate:(id <BIBeaconnectScanningDelegate>)delegate;


/**
 * サービスへ加入する。
 */
- (void)subscribeWithSuccess:(void (^)())success failure:(void (^)(NSError *))failure;

/**
 * ビーコン検知開始
 *
 */
- (void)startScan:(id <BIBeaconnectScanningDelegate>)delegate timeout:(NSTimeInterval)timeout success:(void (^)())success failure:(void (^)(NSError *))failure;


/**
 * ビーコン検知終了
 *
 */
- (void)stopScan;


/**
 * 端末認証
 *
 * @param serviceId サービスコード
 * @param apiKey APIキー
 * @return BOOL
 */
- (BOOL)authenticationForService:(NSString *)serviceId apiKey:(NSString *)apiKey;

/*!
    @discussion 指定されたサーバーへの認証に成功しましたかのを確認する。
    @return 認証済みか
 */
- (BOOL)isAuthorized;


/**
 * モバールフォンIDを取得する。
 *
 *  @return モバールフォンID
 */
- (NSUUID *)mpid;

/**
 * サービスの取得及びサービスへの加入を行う
 *
 * @param serviceId サービスコード
 * @return BOOL
 */
- (BOOL)subscribeToService:(NSString *)serviceId;

/**
 * もっとも近くにあるビーコンの情報を取得
 *
 *  @return もっとも近くにあるビーコン
 */
- (BIBeacon *)nearestBeacon;


/**
 * プッシュトークンの更新
 * @param pushToken プッシュトークン
 * @param serviceId サービスコード
 *
 * @return プッシュトークンの更新結果
 */
- (BOOL)updatePushToken:(NSString *)pushToken serviceId:(NSString *)serviceId;


/**
 * プッシュトークンの更新
 * @param pushToken プッシュトークン
 * @return BeaconDetail
 */
- (void)beaconDetail:(BIBeacon *)beacon success:(void (^)(BIBeaconDetail *))success failure:(void (^)(NSError *))failure;

/**
 * 検知したビーコンの一覧を取得
 *
 *  @return 検知したビーコンリスト
 */
- (NSArray *)detectBeacons;


@property (strong, nonatomic) BIDriversManager *driversManager;

@end
