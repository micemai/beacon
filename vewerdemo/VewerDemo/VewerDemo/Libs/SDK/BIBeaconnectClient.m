//
//  BeaconConnectService.m
//  BeaconConnect
//
//  Created by zhu on 2014/07/17.
//  Copyright (c) 2014年 zhu. All rights reserved.
//


#import "BIBeaconnectClient.h"
#import "BIUtils.h"
#import "BIDeviceIdStore.h"
#import "BIBeaconDBManager.h"
#import "BIFetchServiceInfoRequest.h"
#import "BIFetchMobilePhoneForCheckRequest.h"
#import "BISubscribeToServiceRequest.h"
#import "BIFetchBeaconSumsRequest.h"
#import "BIDriversManager.h"
#import "BIBeaconDetail.h"
#import "BIFetchBeaconDetailWithBeaconId.h"
#import "BIBeaconCollection.h"

//static NSString * const BIBeaconnectApiBaseURL = @"http://172.16.1.98:3000/";
static NSString * const BIBeaconnectApiBaseURL = @"http://beacon.myu-on.net";
//static NSString * const BIBeaconnectApiBaseURL = @"http://172.16.1.53:8081";


@interface BIBeaconnectClient ()
@end

@implementation BIBeaconnectClient {
    NSUUID *_mpid;

	NSURL *_baseURL;
    NSString *_sid;
    NSString *_apiKey;
    NSTimeInterval _timeout;

    BIServiceInfo *_serviceInfo;
	BIMobilePhone *_mobilePhone;
	NSArray *_beaconSummaries;

	NSArray *_sumarries;

    BIBeaconDBManager *_dbMgr;
	BIDriversManager *_driversManager;
	dispatch_queue_t _clientQueue;
	
	id <BIBeaconnectScanningDelegate> _delegate;
}

-(id)initWithServiceId:(NSString *)sid apiKey:(NSString *)apiKey timeout:(NSTimeInterval)timeout delegate:(id <BIBeaconnectScanningDelegate>)delegate
{
    self = [super init];
    if (self) {
		_baseURL = [NSURL URLWithString:BIBeaconnectApiBaseURL];
        _sid = sid;
        _apiKey = apiKey;
        _timeout = timeout;

        _dbMgr = [[BIBeaconDBManager alloc] init];

		_clientQueue = dispatch_queue_create("beaconnct.client.queue", NULL);

		_delegate = delegate;
    }
    return self;
}

- (id)initWithBaseURL:(NSURL *)baseURL serviceId:(NSString *)sid apiKey:(NSString *)apiKey timeout:(NSTimeInterval)timeout delegate:(id <BIBeaconnectScanningDelegate>)delegate
{
    self = [super init];
    if (self) {
		_baseURL = baseURL;
        _sid = sid;
        _apiKey = apiKey;
        _timeout = timeout;

        _dbMgr = [[BIBeaconDBManager alloc] init];

		_clientQueue = dispatch_queue_create("beaconnct.client.queue", NULL);

		_delegate = delegate;
	}
    return self;
}

// private method
- (void)waitForSemaphore:(dispatch_semaphore_t)semaphore
{
	while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
		[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	}
}

// private method
- (BOOL)checkConfig
{
	NSAssert(_baseURL, @"baseURLが設定されていません。");
	NSAssert(_sid, @"sidが設定されていません。");
	NSAssert(_apiKey, @"apiKeyが設定されていません。");

	if (_mpid == nil) {
		[self mpid]; // _mpidが初期化される。
	}

	if (_serviceInfo == nil) { // サービス情報をDB、サーバからロードする。
		if (![self loadServiceInfo]) {
			return NO;
		}
	}

	if (_mobilePhone == nil) { // モバイルフォン情報をDB、サーバからロードする。
		if (![self loadMobilePhone]) {
			if ([self registerMobilePhone]) {
				return [self loadMobilePhone];
			} else {
				return NO;
			}
		}
	}
	return YES;
}

- (void)subscribeWithSuccess:(void (^)())success failure:(void (^)(NSError *))failure
{
	dispatch_async(_clientQueue, ^{
		if (![self checkConfig]) {
			failure([NSError errorWithDomain:@"BeaconnectErrorDomain" code:1 userInfo:nil]);
			return;
		}

		success();
	});
}

//
//- (BOOL)authenticationForService:(NSString *)serviceId apiKey:(NSString *) apiKey
//{
//    NSUUID *mpUUID = [[NSUUID alloc] initWithUUIDString:[self mpid]];
//    BIBeaconnectAuthClient *authClient = [[BIBeaconnectAuthClient alloc] initWithServiceId:serviceId apiKey:apiKey mpid:mpUUID beaconnectApi:_api];
//
//    NSError *error = nil;
//    _authToken = [authClient authenticateWithError:&error];
//
//    if (error != nil || _authToken == nil) {
//        return NO;
//    }
//
//	return YES;
//}
//
//- (BOOL)isAuthorized
//{
//    return _authorized;
//}
//

// private method
- (BOOL)loadServiceInfo
{
	// DBからサービス情報を取得する。
    _serviceInfo = [_dbMgr referServiceInWithSid:[_sid intValue]];

	// サーバから取得しなおす。（失敗したら無視）
	__block BOOL ret = NO;
	dispatch_semaphore_t sem = dispatch_semaphore_create(0);
	__block BIFetchServiceInfoRequest *request = [[BIFetchServiceInfoRequest alloc] init];
	request.successProc = ^(BIBeaconnectClientRequest *req, id result) {
		BIServiceInfo *serviceInfo = result;
		if (_serviceInfo == nil) {
			// 新規
			_serviceInfo = serviceInfo;
			[_dbMgr addServiceInfo:_serviceInfo];
		} else if (![_serviceInfo.version isEqualToString:serviceInfo.version]) {
			// 更新
			_serviceInfo = serviceInfo;
			[_dbMgr updateServiceInfo:_serviceInfo];
		}
		ret = YES;
		dispatch_semaphore_signal(sem);
	};
	request.failureProc = ^(BIBeaconnectClientRequest *req, NSError *error){
		dispatch_semaphore_signal(sem);
	};
	[request fetchWithSid:_sid];
	[self waitForSemaphore:sem]; // sync
	return ret;
}

// private method
- (BOOL)loadMobilePhone
{
	// DBからモバイルフォン情報を取得する。
	_mobilePhone = [_dbMgr referMobilePhoneWithMpid:_mpid serviceId:_serviceInfo.seqId];

	// サーバから取得しなおす。（失敗したら無視）
	__block BOOL ret = NO;
	dispatch_semaphore_t sem = dispatch_semaphore_create(0);
	BIFetchMobilePhoneForCheckRequest *request = [[BIFetchMobilePhoneForCheckRequest alloc] init];
	request.successProc = ^(BIBeaconnectClientRequest *req, id result) {
		BIMobilePhone *mobilePhone = result;
		if (_mobilePhone == nil) {
			_mobilePhone = mobilePhone;
			[_dbMgr addMobilePhone:_mobilePhone];
		} else {
			_mobilePhone.seqId = mobilePhone.seqId;
			[_dbMgr updateMobilePhone:_mobilePhone];
		}
		ret = YES;
		dispatch_semaphore_signal(sem);
	};
	request.failureProc = ^(BIBeaconnectClientRequest *req, NSError *error) {
		dispatch_semaphore_signal(sem);
	};
	[request fetchWithSeqId:_serviceInfo.seqId mpid:_mpid];
	[self waitForSemaphore:sem]; // sync
	return ret;
}

// private method
- (BOOL)registerMobilePhone
{
	__block BOOL ret = NO;
	dispatch_semaphore_t sem = dispatch_semaphore_create(0);
	BISubscribeToServiceRequest *request = [[BISubscribeToServiceRequest alloc] init];
	request.successProc = ^(BIBeaconnectClientRequest *req, id result) {
		ret = YES;
		dispatch_semaphore_signal(sem);
	};
	request.failureProc = ^(BIBeaconnectClientRequest *req, NSError *error) {
		dispatch_semaphore_signal(sem);
	};
	[request subscribeToServiceWithSeqId:_serviceInfo.seqId mpid:_mpid pushToken:_mobilePhone.pushToken];
	[self waitForSemaphore:sem]; // sync
	return ret;
}


- (void)startScan:(id <BIBeaconnectScanningDelegate>)delegate timeout:(NSTimeInterval)timeout success:(void (^)())success failure:(void (^)(NSError *))failure
{
	NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
	[center addObserver:self selector:@selector(didAddedEntry:) name:BIBeaconCollectionAddedEntry object:nil];
	[center addObserver:self selector:@selector(didNearestEntry:) name:BIBeaconCollectionNearestEntry object:nil];
	[center addObserver:self selector:@selector(didExitEntry:) name:BIBeaconCollectionExitedEntry object:nil];

	dispatch_async(_clientQueue, ^{
		_delegate = delegate;
		_timeout = timeout;

		if (![self checkConfig]) {
			failure([NSError errorWithDomain:@"BeaconnectErrorDomain" code:1 userInfo:nil]);
			return;
		}

		if (![self loadBeaconSummaries]) {
			NSLog(@"ビーコンリストの取得に失敗しました。");
			failure([NSError errorWithDomain:@"BeaconnectErrorDomain" code:2 userInfo:nil]);
		}

        dispatch_sync(dispatch_get_main_queue(), ^(){
            //Add method, task you want perform on mainQueue
            //Control UIView, IBOutlet all here
       
            NSSet *targetList = [NSSet setWithArray:_beaconSummaries];
            if (_driversManager == nil) {
                _driversManager = [[BIDriversManager alloc] init];
            }
            [_driversManager startMonitoringWithProximityUUID:[[NSUUID alloc] initWithUUIDString:_serviceInfo.uuid] identifier:[NSString stringWithFormat:@"%d", _serviceInfo.sid] targetBeacons:targetList timeout:_timeout];
            
            success();
            NSLog(@"成功");
            
        });
	});
}

// private
- (BOOL)loadBeaconSummaries
{
	// DBからロードする。
	_beaconSummaries = [_dbMgr referBeaconSummariesWithServiceId:_serviceInfo.seqId];

	// サーバから取得しなおす。（失敗したら無視する。）
	__block BOOL ret = NO;
	dispatch_semaphore_t sem = dispatch_semaphore_create(0);
	BIFetchBeaconSumsRequest *request = [[BIFetchBeaconSumsRequest alloc] init];
	request.successProc = ^(BIBeaconnectClientRequest *req, id result) {
		_beaconSummaries = (NSArray *)result;
        
        // FIXME
        [_beaconSummaries enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            BIBeaconSummary *summary = (BIBeaconSummary *)obj;
            summary.uuid = [[NSUUID alloc] initWithUUIDString:_serviceInfo.uuid];
        }];
        
        
		// DBの内容を上書きする。
		[_dbMgr deleteBeaconSummaryWithServiceId:_serviceInfo.seqId];
		for (BIBeaconSummary *b in _beaconSummaries) {
			[_dbMgr addBeaconSummary:b];
		}

		ret = YES;
		dispatch_semaphore_signal(sem);
	};
	request.failureProc = ^(BIBeaconnectClientRequest *req, NSError *error) {
		dispatch_semaphore_signal(sem);
	};
	[request fetchFromService:_serviceInfo.seqId];
	[self waitForSemaphore:sem]; // sync
	return ret;
}

// private
- (void)saveServiceInfo
{
	[_dbMgr addServiceInfo:_serviceInfo];
}


- (void)stopScan
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:BIBeaconCollectionAddedEntry object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:BIBeaconCollectionNearestEntry object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:BIBeaconCollectionExitedEntry object:nil];

	[_driversManager stopMonitoringWithProximityUUID:[[NSUUID alloc] initWithUUIDString:_serviceInfo.uuid]];
}


/**
 * モバイルフォンIDを取得する。
 *
 *  @return モバイルフォンID
 */
- (NSUUID *)mpid
{
    if (_mpid == nil) {
        BIDeviceIdStore *store = [BIDeviceIdStore shareInstance];
        NSString *mpidStr = [store create];
        _mpid = [[NSUUID alloc] initWithUUIDString:mpidStr];
    }
    return _mpid;
}

///**
// * プッシュトークン更新
// */
//- (BOOL)updatePushToken:(NSString *)pushToken serviceId:(NSString *)serviceId
//{
//    _currentServiceInfo = [_dbMgr referServiceInWithSid:[serviceId intValue]];
//    
//    if (_currentServiceInfo == nil) {
//        return false;
//    }
//    
//    BIMobilePhone *localMp = [_dbMgr referMobilePhoneWithMpid:[self mpid] serviceId:[serviceId intValue]];
//    if (localMp == nil) {
//        return false;
//    }
//        
//    NSError *_error = nil;
//    NSString *seqIdString =[NSString stringWithFormat:@"%d",  (int)_currentServiceInfo.seqId];
//
//    BIMobilePhone *mp = [self updatePushTokenForService:seqIdString
//                                                mpSeqId:localMp.seqId
//                                              pushToken:pushToken
//                                                  error:&_error];
//
//    if (mp != nil) {
//        [[BIBeaconDBManager sharedInstance] updateMobilePhone:mp];
//        return YES;
//    }
//    return NO;
//}

- (void)beaconDetail:(BIBeacon *)beacon success:(void (^)(BIBeaconDetail *))success failure:(void (^)(NSError *))failure
{
	__weak BIBeaconnectClient *weakSelf = self;
	dispatch_async(_clientQueue, ^{
		if (![self checkConfig]) {
			failure([NSError errorWithDomain:@"BeaconnectErrorDomain" code:1 userInfo:nil]);
			return;
		}
		
		// 対象のビーコンサマリを検索する。
		BIBeaconSummary *targetBeaconSummary = nil;
		for (BIBeaconSummary *s in _beaconSummaries) {
            if ([[s.uuid UUIDString] isEqualToString:[beacon.uuid UUIDString]]
                && [s.major intValue] == [beacon.major intValue]
                && [s.minor intValue] == [beacon.minor intValue]) {
             
			//if (s.bid == beacon.bid.integerValue) {
				targetBeaconSummary = s;
				break;
			}
		}
		
		if (!targetBeaconSummary) {
			success(nil);
		} else {
			success([weakSelf searchBeaconDetailWithBeaconSummary:targetBeaconSummary]);
		}
	});
}

- (BIBeaconDetail *)searchBeaconDetailWithBeaconSummary:(BIBeaconSummary *)beaconSummary
{
	// サーバから取得する。
	dispatch_semaphore_t sem = dispatch_semaphore_create(0);
	BIFetchBeaconDetailWithBeaconId *request = [[BIFetchBeaconDetailWithBeaconId alloc] init];
	request.successProc = ^(BIBeaconnectClientRequest *req, id result) {
		dispatch_semaphore_signal(sem);
	};
	request.failureProc = ^(BIBeaconnectClientRequest *req, NSError *error) {
		dispatch_semaphore_signal(sem);
	};
	[request fetchBeaconDetailWithBeaconId:beaconSummary.seqId];
	[self waitForSemaphore:sem]; // sync
	return request.result;
}


////////////////////////////////////////////////////////////////////////////////
#pragma mark delegate
////////////////////////////////////////////////////////////////////////////////

- (void)didAddedEntry:(NSNotification *)notification
{
	if ([_delegate respondsToSelector:@selector(client:didAddDetectList:)]) {
		[_delegate client:self didAddDetectList:notification.userInfo[BIBeaconCollectionAddedEntryKey]];
	}
}

- (void)didNearestEntry:(NSNotification *)notification
{
	if ([_delegate respondsToSelector:@selector(client:didUpdateNearest:)]) {
		[_delegate client:self didUpdateNearest:notification.userInfo[BIBeaconCollectionNearestEntryKey]];
	}
}

- (void)didExitEntry:(NSNotification *)notification
{
	if ([_delegate respondsToSelector:@selector(client:didOutOfRange:)]) {
		[_delegate client:self didOutOfRange:notification.userInfo[BIBeaconCollectionExitedEntryKey]];
	}
}

@end
