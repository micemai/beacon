//
//  Beacon.h
//  BeaconConnect
//
//  Created by zhu on 2014/07/17.
//  Copyright (c) 2014年 zhu. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "BIBeaconAction.h"

/*!
    @class BIBeacon
 
    @discussion BIBeaconクラスはビーコンの属性を定義する。
 */
@interface BIBeacon : NSObject

/*!
    @property uuid
 
    @discussion アドバタイズしているビーコンのUUID
 */
@property (strong, nonatomic) NSUUID *uuid;

/*!
    @property major
 
    @discussion ビーコンmajor
 */
@property (strong, nonatomic) NSNumber *major;

/*!
    @property minor
 
    @discussion ビーコンminor
 */
@property (strong, nonatomic) NSNumber *minor;

/*!
    @property rssi
 
    @discussion 電波の強度
 */
@property (assign, nonatomic) NSInteger rssi;

/*!
    @property detectionDate
    
    @discussion 検知日付
*/
@property (strong, nonatomic) NSDate *detectionDate;


///*!
//    @property action
// 
//    @discussion アクション情報
// */
//@property (strong, nonatomic) BIBeaconAction *action;
// 
@end
