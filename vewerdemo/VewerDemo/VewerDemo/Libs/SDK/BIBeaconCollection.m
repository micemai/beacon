//
//  BIBeaconCollection.m
//  Beaconnect-ios-sdk
//
//  Created by Toshiki Tsubouchi on 8/8/14.
//  Copyright (c) 2014 Toshiki Tsubouchi. All rights reserved.
//

#import "BIBeaconCollection.h"
#import "BIBeaconData.h"
#import "BIBeaconSummary.h"
#import "BIBeaconDBManager.h"
#import "BIDeviceIdStore.h"
#import "BIUploadAccessPointLogsWithDetectedAt.h"
#import "NSNotificationCenter+MainThread.h"

#import <CoreLocation/CoreLocation.h>

NSString * const BIBeaconCollectionAddedEntry = @"BIBeaconCollectionAddedEntry";
NSString * const BIBeaconCollectionAddedEntryKey = @"BIBeaconCollectionAddedEntryKey";
NSString * const BIBeaconCollectionExitedEntry = @"BIBeaconCollectionExitedEntry";
NSString * const BIBeaconCollectionExitedEntryKey = @"BIBeaconCollectionExitedEntryKey";
NSString * const BIBeaconCollectionNearestEntry = @"BIBeaconCollectionNearestEntry";
NSString * const BIBeaconCollectionNearestEntryKey = @"BIBeaconCollectionNearestEntryKey";

@implementation BIBeaconCollection

- (instancetype)init {
	self = [super init];
	if (self) {
		_detectedBeacons = [NSMutableDictionary dictionary];
        _activeBeaconIDs = [NSMutableArray array];
		_targetBeacons = nil;
		_timeout = 60;
	}
	return self;
}

- (instancetype)initWithTargetBeacons:(NSSet *)targetBeacons timeout:(NSTimeInterval)timeout
{
	self = [super init];
	if (self) {
		_detectedBeacons = [NSMutableDictionary dictionary];
        _activeBeaconIDs = [NSMutableArray array];
		_targetBeacons = targetBeacons;
		_timeout = timeout;
	}
	return self;
}


- (BIBeaconSummary *)findBeaconSummaryWithCLBeacon:(CLBeacon *)beacon
{
    // FIXME
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@ and major == %@ and minor == %@",
                              beacon.proximityUUID, beacon.major, beacon.minor];
    NSSet *filteredSet = [_targetBeacons filteredSetUsingPredicate:predicate];
    
    return (filteredSet == nil) ? nil : [filteredSet anyObject];
}


- (void)updateBeacons:(NSArray *)beacons
{
    for (CLBeacon *clBeacon in beacons) {
        
        NSString *identifier = [NSString stringWithFormat:@"%@%@%@",
                                [[clBeacon.proximityUUID UUIDString] lowercaseString],
                                [clBeacon.major stringValue],
                                [clBeacon.minor stringValue] ];
        
        BIBeaconData *beaconData = [self.detectedBeacons objectForKey:identifier];
        
        if (!beaconData) {   // 検知リストに存在してない場合]
            BILog(@"検知リストに存在しないビーコンを検知しました。 uuid=%@ major=%@ minor=%@", [clBeacon.proximityUUID UUIDString], [NSString stringWithFormat:@"%X",[clBeacon.major intValue]], [NSString stringWithFormat:@"%X",[clBeacon.minor intValue]]);

            beaconData = [[BIBeaconData alloc] init];
            beaconData.identifier = identifier;

            BIBeacon *beacon = [[BIBeacon alloc] init];
            beacon.uuid = clBeacon.proximityUUID;
            beacon.major = clBeacon.major;
            beacon.minor = clBeacon.minor;
            if (clBeacon.rssi == 0) {
                beacon.rssi = -256;
            } else {
                beacon.rssi = clBeacon.rssi;
            }
            beacon.rssi = clBeacon.rssi;
            beacon.detectionDate = [NSDate date];
            beaconData.beacon = beacon;
            beaconData.lastRssi = clBeacon.rssi;
        } else {
            // ビーコンデータを更新する
            if (clBeacon.rssi == 0) {
                if (beaconData.lastRssi == 0) {
                    beaconData.beacon.rssi = -256;
                }
            } else {
                beaconData.beacon.rssi = clBeacon.rssi;
            }
            beaconData.beacon.detectionDate = [NSDate date];
        }
        // 検知リスト更新
        [self.detectedBeacons setObject:beaconData forKey:identifier];
        
        // 有効チェック
        if (![self existActiveBeaconIDs:identifier]) {
            // 有効ビーコン一覧に存在しない
            
            BIBeaconSummary *summary = [self findBeaconSummaryWithCLBeacon:clBeacon];
            if (summary) {
                
                // targetに存在しているので閾値チェック
                if (clBeacon.rssi < summary.threshold) {
                    BILog(@"RSSIの閾値に足りていないため、有効検知リストに追加しません。 ビーコンのRSSI=%ld, 閾値=%ld", (long)clBeacon.rssi, (long)summary.threshold);
                    continue;
                } else {
                    // 閾値を超えているため有効ビーコン一覧に追加
                    [self.activeBeaconIDs addObject:identifier];
                    
                   // beaconData.beacon.action = summary.action;
                    // ログを書き込む
                    [[BIBeaconDBManager sharedInstance] addBeaconLog:[self detectLogWithBeaconData:beaconData]];
                    
                    
                    // そろったむねを通知する。
                    BILog(@"新たに有効なビーコンを検知しました。検知情報をアプリへ送ります uuid=%@ major=%@ minor=%@", [beaconData.beacon.uuid UUIDString], [NSString stringWithFormat:@"%X",[beaconData.beacon.major intValue]], [NSString stringWithFormat:@"%X",[beaconData.beacon.minor intValue]]);
                    
                    NSDictionary *userInfo = @{ BIBeaconCollectionAddedEntryKey : beaconData.beacon };
                    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadName:BIBeaconCollectionAddedEntry object:self userInfo:userInfo];

                }
            }
        }
    }
    
    // 最寄りチェック
    [self checkNearest];
}


- (NSArray *)activeBeacons
{
    NSMutableArray *beacons = [NSMutableArray array];
    
    for (NSString *activeBeaconId in self.activeBeaconIDs) {
        
        BIBeaconData *activeBd = [self.detectedBeacons objectForKey:activeBeaconId];
        if (activeBd) {
            
			BIBeaconSummary *verifiedSummary = nil;
			for (BIBeaconSummary *summary in _targetBeacons) {
				if ([[summary.uuid UUIDString] isEqualToString:[activeBd.beacon.uuid UUIDString]]
                    && [summary.major intValue] == [activeBd.beacon.major intValue]
                    && [summary.minor intValue] == [activeBd.beacon.minor intValue]) {
					verifiedSummary = summary;
					break;
				}
			}
			if (verifiedSummary) {
                if (activeBd.beacon.rssi > verifiedSummary.threshold) {
                    activeBd.summary = verifiedSummary;
                    // activeBd.beacon.action = verifiedSummary.action;
                    
                    [beacons addObject:activeBd];
                }
			}
        }
    }
    
    return beacons;
}

- (BOOL)existActiveBeaconIDs:(NSString *)identifier
{
    for (NSString *activeId in self.activeBeaconIDs) {
        if ([activeId isEqualToString:identifier]) {
            return YES;
        }
    }
    return NO;
}

- (void)update
{
    //BILog(@"定期チェック 開始");
    for (NSUUID *key in [self.detectedBeacons allKeys]) {
        // ビーコン情報取得
        BIBeaconData *beaconData = [self.detectedBeacons objectForKey:key];
        
        // 長い時間で検出してくれないと、ビーコンを除去する。
        NSTimeInterval diff = [[NSDate date] timeIntervalSinceDate:beaconData.beacon.detectionDate];
        //BILog(@"前回検出日時からのインターバル: \"%lf\"", diff);
        
        BOOL isTimeout = diff > _timeout;
        //BILog(@"diff > _timeout: %@", isTimeout ? @"YES" : @"NO");
        if (isTimeout) {
            
            // ログを書き込む
            [[BIBeaconDBManager sharedInstance] addBeaconLog:[self detectLogWithBeaconData:beaconData]];

            
            // 範囲外通知
            //            [[NSNotificationCenter defaultCenter] postNotificationName:BIBeaconCollectionExitedEntry object:self userInfo:@{ BIBeaconCollectionExitedEntryKey : beaconData.beacon }];
            // post notification on main thread
            NSDictionary *userInfo = @{ BIBeaconCollectionExitedEntryKey : beaconData.beacon };
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadName:BIBeaconCollectionExitedEntry object:self userInfo:userInfo];

            // リストから削除
            [self.activeBeaconIDs removeObject:key];
            [self.detectedBeacons removeObjectForKey:key];
            
            if (self.nearestBeaconData != nil) {
                if (beaconData.beacon.uuid == self.nearestBeaconData.beacon.uuid
                    && [beaconData.beacon.major isEqualToNumber:self.nearestBeaconData.beacon.major]
                    && [beaconData.beacon.minor isEqualToNumber:self.nearestBeaconData.beacon.minor]) {
                    
                    self.nearestBeaconData = nil;
                }
            }
            
            BILog(@"タイムアウトしました。uuid=%@ major=%@ minor=%@", [beaconData.beacon.uuid UUIDString], beaconData.beacon.major, beaconData.beacon.minor);
        }
    }
}

- (void)checkNearest
{
    // リストが空の場合、何もしない
    if (self.activeBeacons == nil || [self.activeBeacons count] <= 0) return;
    
    // beaconsからRSSIが一番大きいものを返す
    NSSortDescriptor *sortDescriptorName = [NSSortDescriptor sortDescriptorWithKey:@"beacon.rssi" ascending:NO];
    NSArray *sortActiveBeacons = [self.activeBeacons sortedArrayUsingDescriptors:@[sortDescriptorName]];
    
    BILog(@"----- 最寄 ランキング -----");
    for (NSInteger i = 0; i < sortActiveBeacons.count; i++) {
        NSInteger ranking = i + 1;
        BIBeaconData *b = sortActiveBeacons[i];
        BILog(@"第%d位: [%@][%@][%@][%d]", ranking, [b.beacon.uuid UUIDString], [NSString stringWithFormat:@"%X",[b.beacon.major intValue]], [NSString stringWithFormat:@"%X", [b.beacon.minor intValue]],
              b.beacon.rssi);
    }
    BILog(@"-------------------------");
		
    // 今のNo.1を取得する。
    BIBeaconData *bd = [sortActiveBeacons objectAtIndex:0];
    
    NSDate *now = [NSDate date];
    BOOL shouldNotify = NO;
    if (_nearestBeaconData) {
        // 前回のNo.1がいる場合
        
        // 同じビーコンだったら、変更しない。
//        if ([[bd.beacon.uuid UUIDString] isEqualToString:[_nearestBeaconData.beacon.uuid UUIDString]]
//            && [bd.beacon.major isEqualToNumber:_nearestBeaconData.beacon.major]
//            && [bd.beacon.minor isEqualToNumber:_nearestBeaconData.beacon.minor]) {
//            
//            BILog(@"前回と同じビーコンですので、最寄ビーコンを更新しません。");
//            return;
//        }
//        
        // 占有期限をすぎているか調べる
        NSTimeInterval diff = [now timeIntervalSinceDate:_nearestBeaconExpiredDate];
        BILog(@"占有期限日時=%@, 今回=%@, 差=%lf", _nearestBeaconExpiredDate, now, diff);
        if (diff > 0) {
            // 占有期限を過ぎている場合
            BILog(@"占有期限を過ぎているため、再度最寄ビーコンを調べます。");
            
            // 前回のNoと変わっているか調べる
            BILog(@"前回No.1=%@, 今回No.1=%@", _nearestBeaconData, bd);
            if ([[bd.beacon.uuid UUIDString] isEqualToString:[_nearestBeaconData.beacon.uuid UUIDString]]
                && [bd.beacon.major isEqualToNumber:_nearestBeaconData.beacon.major]
                && [bd.beacon.minor isEqualToNumber:_nearestBeaconData.beacon.minor]) {
                // 変わっていない場合は
                 shouldNotify = YES;
                BILog(@"前回と変わっていないと判定しました。");
            } else {
                // 変わっている場合は
                BILog(@"前回と変わっていると判定しました。");
                BILog(@"一番近いビーコンが変更になりました。");
                // 通知が必要
                shouldNotify = YES;
                
                _nearestBeaconExpiredDate = [NSDate dateWithTimeInterval:bd.summary.exclusiveTime sinceDate:now];
                _nearestBeaconData = bd;
            }
        } else {
            // 占有期限を過ぎていない場合
            BILog(@"占有期限を過ぎていないため、最寄ビーコンを更新しません。");
        }
    } else {
        // 前回のNo.1がいない場合
        // 自分がNo.1でいられる期限をセットする。
        _nearestBeaconExpiredDate = [NSDate dateWithTimeInterval:bd.summary.exclusiveTime sinceDate:now];
        _nearestBeaconData = bd;
        shouldNotify = YES;
        BILog(@"検知日時=%@, 占有期限日時=%@, 占有可能秒数=%d", now, _nearestBeaconExpiredDate, bd.summary.exclusiveTime);
        BILog(@"一番近いビーコンが変更になりました。");
    }
    
    if (shouldNotify) {
        BILog(@"通知が必要なので通知を出します。");
        
        [[BIBeaconDBManager sharedInstance] addBeaconLog:[self detectLogWithBeaconData:bd]];
        
        NSDictionary *userInfo = @{ BIBeaconCollectionNearestEntryKey : _nearestBeaconData.beacon };
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadName:BIBeaconCollectionNearestEntry object:self userInfo:userInfo];
 
    }
}




- (BIBeaconLog *)detectLogWithBeaconData:(BIBeaconData *)data
{
 
    if (data == nil) return nil;
    
    BIBeaconLog *beaconLog = [[BIBeaconLog alloc] init];
    beaconLog.detectedDate = data.beacon.detectionDate;
    beaconLog.accessDeviceType = @"servicer_app";
    beaconLog.mpid = [BIDeviceIdStore shareInstance].deviceId;
    beaconLog.macAddress = @"";
    beaconLog.uuid = [[data.beacon.uuid UUIDString] lowercaseString];
//    beaconLog.bid = [data.bid integerValue];
    beaconLog.bid = 0;
    beaconLog.major = [data.beacon.major integerValue];
    beaconLog.minor = [data.beacon.minor integerValue];
    beaconLog.rssi = data.beacon.rssi;
//    beaconLog.batteryVoltage = data.batteryVoltage;
//   beaconLog.txPower = data.txPower;
//    beaconLog.temperature = data.temperature;
    beaconLog.humidity = 0;
    beaconLog.gyroX = 0;
    beaconLog.gyroY = 0;
    beaconLog.gyroZ = 0;
    
    return beaconLog;
}



- (void)sendbeaconLog
{
    NSArray *beaconLogs = [[BIBeaconDBManager sharedInstance] fetechBeaconLogs];
    [beaconLogs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        BIBeaconLog *log = (BIBeaconLog *)obj;
        __block BIUploadAccessPointLogsWithDetectedAt *request = [[BIUploadAccessPointLogsWithDetectedAt alloc] init];
        request.successProc = ^(BIBeaconnectClientRequest *req, id result) {
             [[BIBeaconDBManager sharedInstance] deleteBeaconLogWithSeqId:log.seqId];
        };
        request.failureProc = ^(BIBeaconnectClientRequest *req, NSError *error) {
        };
    [request uploadAccessPointLogsWithDetectedAt:log.detectedDate accessDeviceType:log.accessDeviceType mpid:log.mpid macAddress:log.macAddress uuid:log.uuid bid:log.bid major:log.major minor:log.minor rssi:log.rssi batteryV:log.batteryVoltage txPower:log.txPower temperature:log.temperature humidity:log.humidity accelX:log.gyroX accelY:log.gyroY accelZ:log.gyroZ];
    }];
   
}



- (void)debug_sendLocalNotificationForMessage:(NSString *)message
{
    UILocalNotification *localNotification = [UILocalNotification new];
    localNotification.alertBody = message;
    localNotification.fireDate = [NSDate date];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

@end
