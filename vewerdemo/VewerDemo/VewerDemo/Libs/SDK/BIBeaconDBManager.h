//
//  BIBeaconDBManager.h
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/25.
//
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDatabaseQueue.h>
#import "BIServiceInfo.h"
#import "BIMobilePhone.h"
#import "BIBeaconLog.h"
#import "BIBeaconSummary.h"
#import "BIToken.h"

@interface BIBeaconDBManager : NSObject

@property (strong, nonatomic) FMDatabaseQueue *dbQueue;

+ (instancetype)sharedInstance;


// ServiceInfo
- (BIServiceInfo *)addServiceInfo:(BIServiceInfo *)serverInfo;
- (BIServiceInfo *)updateServiceInfo:(BIServiceInfo *)serviceInfo;
- (BIServiceInfo *)referServiceInWithSid:(int)sid;
- (NSInteger)referServiceIdWithUuid:(NSUUID *)uuid;

// MobilePhone
- (BIMobilePhone *)addMobilePhone:(BIMobilePhone *)mobile;
- (BIMobilePhone *)updateMobilePhone:(BIMobilePhone *)mobile;
- (BIMobilePhone *)referMobilePhoneWithMpid:(NSUUID *)mpid serviceId:(int)serviceId;

// BeaconSummary
- (BIBeaconSummary *)addBeaconSummary:(BIBeaconSummary *)summary;
- (BOOL)deleteBeaconSummaryWithServiceId:(int) serviceId;
- (NSArray *)referBeaconSummariesWithServiceId:(int) serviceId;

// Log
- (BIBeaconLog *)addBeaconLog:(BIBeaconLog*)log;
- (BIBeaconLog *)updateBeaconLog:(BIBeaconLog *)log;
- (BOOL)deleteBeaconLogIsSent;
- (NSArray *)fetechBeaconLogs;
- (BOOL)deleteBeaconLogWithSeqId:(int)seqId;


// Token
- (BIToken *)addToaken:(BIToken *)token;
- (BIToken *)updateToken:(BIToken *)token;

@end
