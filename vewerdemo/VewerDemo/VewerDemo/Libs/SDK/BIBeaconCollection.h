//
//  BIBeaconCollection.h
//  Beaconnect-ios-sdk
//
//  Created by Toshiki Tsubouchi on 8/8/14.
//  Copyright (c) 2014 Toshiki Tsubouchi. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const BIBeaconCollectionAddedEntry;
extern NSString * const BIBeaconCollectionAddedEntryKey;
extern NSString * const BIBeaconCollectionExitedEntry;
extern NSString * const BIBeaconCollectionExitedEntryKey;
extern NSString * const BIBeaconCollectionNearestEntry;
extern NSString * const BIBeaconCollectionNearestEntryKey;

@class BIBeacon;
@class BIBeaconData;


/**
 * このコレクションはスレッドセーフではないため、複数のスレッドで扱ってはいけない。
 * やむを得ず複数のスレッドで扱う場合は必ず排他制御を行うこと。
 */
@interface BIBeaconCollection : NSObject

/**
 * 内部保持用リスト
 *
 */
@property (strong, nonatomic, readonly) NSMutableDictionary *detectedBeacons;
@property (strong, nonatomic, readonly) NSMutableArray *activeBeaconIDs;
@property (nonatomic, strong) NSSet *targetBeacons;
@property (nonatomic, assign) NSTimeInterval timeout;
@property (strong, nonatomic) BIBeaconData *nearestBeaconData;
@property (strong, nonatomic) NSDate *nearestBeaconExpiredDate;

/**
 *
 *
 */
@property (nonatomic, readonly) NSArray *activeBeacons;

/**
 *
 * @param [NSSet *] targetBeacons 対象ビーコン一覧
 * @param [NSSet *] timeout ビーコンの通知が来なくなってからリストから削除するまでの時間
 */
- (instancetype)initWithTargetBeacons:(NSSet *)targetBeacons timeout:(NSTimeInterval)timeout;

/**
 * コレクションを更新する。
 * コレクションを更新した結果、追加されたビーコン、削除されたビーコンが通知される。
 */
//- (BIBeaconData *)updateBeaconWithPeripheralIdentifier:(NSUUID *)peripheralIdentifier data:(NSData *)data rssi:(NSInteger)rssi;
- (void)updateBeacons:(NSArray *)beacons;

/**
 * 更新する。
 */
- (void)update;

/**
 *
 */
- (void)sendbeaconLog;

@end
