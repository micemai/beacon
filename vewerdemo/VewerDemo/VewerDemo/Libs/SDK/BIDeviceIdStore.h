//
//  DeviceIdStore.h
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/19.
//
//

#import <Foundation/Foundation.h>

/*
 * 端末ID格納クラス
 */
@interface BIDeviceIdStore : NSObject

/**
 * 端末ID
 */
@property (readonly, nonatomic) NSString *deviceId;

/**
 * DeviceIdStoreの唯一のインスタンスを返す。
 */
+ (instancetype)shareInstance;

/**
 * キーチェーンにデーバスIDが保存されているかを確認する。
 *
 * @return YES あり
 *          NO なし
 */
- (BOOL)exists;

/**
 * デーバスIDを発行する。
 * @return YES あり
 *          NO なし
 */
- (NSString *)create;;

@end
