//
//  BIServiceInfo.h
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/24.
//
//

#import <Foundation/Foundation.h>

@interface BIServiceInfo : NSObject

@property (assign, nonatomic) NSInteger seqId;
@property (assign, nonatomic) NSInteger sid;
@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) NSString *version;

@end
