//
//  BIBeaconSummary.h
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/25.
//
//

#import <Foundation/Foundation.h>
//#import "BIBeaconAction.h"

@interface BIBeaconSummary : NSObject
@property (assign, nonatomic) NSInteger seqId;
@property (assign, nonatomic) NSInteger serviceId;
@property (strong, nonatomic) NSUUID   *uuid;
@property (strong, nonatomic) NSNumber *major;
@property (strong, nonatomic) NSNumber *minor;
@property (assign, nonatomic) NSInteger bid;
@property (assign, nonatomic) NSInteger threshold;
@property (assign, nonatomic) NSInteger exclusiveTime;
//@property (strong, nonatomic) BIBeaconAction *action;
@end
