//
//  BIDriversManager.h
//  Beaconnect-ios-sdk
//
//  Created by Toshiki Tsubouchi on 8/8/14.
//  Copyright (c) 2014 Toshiki Tsubouchi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "BIBeaconCollection.h"

/**
 * このクラスはBLE関連の通知を処理し、モデルに変換する。
 */
@interface BIDriversManager : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic, readonly) CLBeaconRegion *beaconRegion;
@property (strong, nonatomic, readonly) CLLocationManager *locationManager;
@property (strong, nonatomic, readonly) BIBeaconCollection *beaconCollection;

- (void)startMonitoringWithProximityUUID:(NSUUID *)proximityUUID identifier:identifier targetBeacons:(NSSet *)targetBeacons timeout:(NSTimeInterval)timeout;
- (void)stopMonitoringWithProximityUUID:(NSUUID *)proximityUUID;

@end
