//
//  BIBeaconnectClientRequest.m
//  Beacon
//
//  Created by Toshiki Tsubouchi on 8/2/14.
//

#import "BIBeaconnectClientRequest.h"

#import <AFNetworking/AFNetworking.h>

#import "BIBeaconnectError.h"

#define BASE_URL @"http://beacon.myu-on.net"
//#define BASE_URL @"http://172.16.1.53:8081/"

static NSString * const BEACONNECT_STATUS_KEY = @"X-Beaconnect-Status";

@implementation BIBeaconnectClientRequest

- (NSURL *)baseURL
{
	return [NSURL URLWithString:BASE_URL];
}

- (NSURL *)APIURLWithAPIPath:(NSString *)path
{
	return [self.baseURL URLByAppendingPathComponent:path];
}

- (NSURL *)APIURLWithAPIPath:(NSString *)path parameters:(NSDictionary *)parameters
{
	NSMutableString *path2 = [NSMutableString string];

	NSArray *pathComps = [path pathComponents];
	for (NSString *comp in pathComps) {
		if ([comp isEqualToString:@"/"]) {
			continue;
		}

		NSString *val = nil;
		if (comp.length > 2 && [comp characterAtIndex:0] == ':') {
			NSString *key = [comp substringFromIndex:1];
			val = [self URLEncode:parameters[key]];
		}
		if (val == nil) {
			val = comp;
		}
		[path2 appendString:@"/"];
		[path2 appendString:val];
	}
	return [self.baseURL URLByAppendingPathComponent:path2];
}

- (NSMutableURLRequest *)URLRequestWithAPIPath:(NSString *)path
{
	return [NSMutableURLRequest requestWithURL:[self APIURLWithAPIPath:path] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:8];
}

- (NSString *)URLEncode:(NSString *)str
{
	if (str == nil) {
		return nil;
	}

	NSString *escapedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
																				  kCFAllocatorDefault,
																				  (CFStringRef)str,
																				  NULL,
																				  (CFStringRef)@"!*'();:@&=+$,/?%#[]",
																				  kCFStringEncodingUTF8));
	return escapedString;
}

- (BOOL)isBeaconnectErrorResponse:(NSURLResponse *)response
{
	return [[((NSHTTPURLResponse *)response) allHeaderFields][BEACONNECT_STATUS_KEY] isEqualToString:@"error"];
}

- (void)setBeaconnectErrorObjectFromResponseObject:(id)responseObject
{
	if ([responseObject isKindOfClass:[NSDictionary class]]) {
		// TODO エラーオブジェクトを設定する。
		NSLog(@"error");
		self.error = [BIBeaconnectError errorWithDomain:@"BeaconnectErrorDomain" code:1 userInfo:nil];
	} else {
		NSLog(@"illegal");
		self.error = [BIBeaconnectError errorWithDomain:@"BeaconnectErrorDomain" code:-1 userInfo:nil];
	}
}

- (void)setBeaconnectErrorAsProtocolError
{
	self.HTTPStatus = ((NSHTTPURLResponse *)self.task.response).statusCode;
	
	self.error = [BIBeaconnectError errorWithDomain:@"BeaconnectErrorDomain" code:-1 userInfo:nil];
}

- (void)parseResponseWithTask:(NSURLSessionTask *)task responseObject:(id)responseObject
					  success:(id (^)(NSURLSessionTask *task, id responseObject))successProc
					  failure:(void (^)(NSURLSessionTask *task, NSError *error))failureProc
{
	self.HTTPStatus = ((NSHTTPURLResponse *)task.response).statusCode;

	// 呼び出し成功
	if ([self isBeaconnectErrorResponse:task.response]) {
		// APIエラー
		[self setBeaconnectErrorObjectFromResponseObject:responseObject];
		failureProc(task, self.error);
		if (self.failureProc) {
			self.failureProc(self, self.error);
		}
	} else {
		// 成功
		self.result = successProc(task, responseObject);

		// コールバック
		if (self.error == nil) {
			if (self.successProc) {
				self.successProc(self, self.result);
			}
		} else {
			if (self.failureProc) {
				self.failureProc(self, self.error);
			}
		}
	}
}

- (void)handleErrorWithTask:task error:error
{
	self.HTTPStatus = ((NSHTTPURLResponse *)self.task.response).statusCode;

	self.error = error;
	if (self.failureProc) {
		self.failureProc(self, self.error);
	}
}
@end
