//
//  BIFetchAPIKeyRequest.h
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIBeaconnectClientRequest.h"

@interface BIFetchAccessTokenFromServer : BIBeaconnectClientRequest

- (void)fetchAccessTokenFromServer:(NSString *)serviceId
                              mpid:(NSUUID *)mpid
                        credential:(NSString *)credential;

@end
