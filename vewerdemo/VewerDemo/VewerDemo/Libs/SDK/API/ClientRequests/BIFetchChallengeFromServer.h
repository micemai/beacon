//
//  BIFetchChallengeFromServer
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIBeaconnectClientRequest.h"

@interface BIFetchChallengeFromServer : BIBeaconnectClientRequest

- (void)fetchChallengeFromServer:(NSString *)serviceId
                              mpid:(NSUUID *)mpid;

@end
