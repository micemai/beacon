//
//  BIFetchChallengeFromServer
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIFetchChallengeFromServer.h"

@implementation BIFetchChallengeFromServer

- (void)fetchChallengeFromServer:(NSString *)serviceId
                              mpid:(NSUUID *)mpid
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/auth/service/challenges"];
	NSLog(@"%@", URL);
    
    NSDictionary *params = @{
                             @"auth_id" :  serviceId,
                             @"end_point_id" : [mpid UUIDString]
                             };
    
	self.task = [manager POST:URL.absoluteString parameters:params
					 success:^(NSURLSessionDataTask *task, id responseObject) {
						 [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
							 // NSLog(@"%@", responseObject);
							 if ([responseObject isKindOfClass:[NSDictionary class]]) {
								 return responseObject[@"token"];
							 } else {
								 [self setBeaconnectErrorAsProtocolError];
								 NSLog(@"Protocol Error: object=%@", responseObject);
								 return nil;
							 }
						 } failure:^(NSURLSessionTask *task, NSError *error) {
							 
						 }];
					 } failure:^(NSURLSessionDataTask *task, NSError *error) {
						 // 呼び出し失敗
						 [self handleErrorWithTask:task error:error];
					 }];

}

@end
