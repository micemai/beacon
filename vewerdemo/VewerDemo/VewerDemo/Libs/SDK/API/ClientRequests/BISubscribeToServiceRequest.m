//
//  BISubscribeToService.m
//  TGC
//
//  Created by sasajima on 2014/08/07.
//

#import "BISubscribeToServiceRequest.h"

@implementation BISubscribeToServiceRequest

- (void)subscribeToServiceWithSeqId:(NSInteger)seqId
                               mpid:(NSUUID *)mpid
                          pushToken:(NSString *)pushToken
{
 	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/services/:seq_id/mobile_phones" parameters:@{ @"seq_id" : [NSString stringWithFormat:@"%d", seqId] }];
	NSLog (@"%@", URL);

    NSString * mpidString = [[mpid UUIDString] lowercaseString];
  	NSMutableDictionary *params = [@{
									@"mpid" : mpidString,
									@"os_type" : @"IOS",
									} mutableCopy];
	
	params[@"push_token"] = pushToken != nil ?  pushToken : @"-";

	self.task = [manager POST:URL.absoluteString parameters:params
                     success:^(NSURLSessionDataTask *task, id responseObject) {
                         [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
                             BILog(@"NSURLSessionDataTask: %@", task.response);
                             NSHTTPURLResponse *resp = (NSHTTPURLResponse *)task.response;
                             if (resp.statusCode == 201) {
                                 NSString *location = resp.allHeaderFields[@"Location"];
                                 return location;
                             } else {
                                 [self setBeaconnectErrorAsProtocolError];
                                 NSLog(@"Protocol Error: object=%@", responseObject);
                                 return nil;
                             }
                         } failure:^(NSURLSessionTask *task, NSError *error) {
                             
                         }];
                     } failure:^(NSURLSessionDataTask *task, NSError *error) {
                         // 呼び出し失敗
                         [self handleErrorWithTask:task error:error];
                     }];
}

- (NSString *)location {
    return self.result;
}

@end
