//
//  BIFetchChallengeFromServer
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIFetchBeaconDetailWithBeaconId.h"

@implementation BIFetchBeaconDetailWithBeaconId

- (void)fetchBeaconDetailWithBeaconId:(NSInteger)beaconId
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/beacons/:id" parameters:@{ @"id" : [NSString stringWithFormat:@"%ld", (long)beaconId] }];
	NSLog(@"%@", URL);
    
	self.task = [manager GET:URL.absoluteString parameters:nil
					 success:^(NSURLSessionDataTask *task, id responseObject) {
						 [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
							 // NSLog(@"%@", responseObject);
                             if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                 BIBeaconDetail *beaconDetail = [[BIBeaconDetail alloc] init];
                                 beaconDetail.beaconId = [responseObject[@"id"] longValue];
                                 beaconDetail.macAddress = responseObject[@"mac"];
                                 beaconDetail.bid = [responseObject[@"bid"] integerValue];
                                 beaconDetail.major = [responseObject[@"major"] integerValue];
                                 beaconDetail.minor = [responseObject[@"minor"] integerValue];
                             
                                 
                                 BIBeaconAction *beaconAction = [[BIBeaconAction alloc] init];
                                 beaconAction.actionId = (long)[responseObject valueForKeyPath:@"detect_action.id"];
                                 beaconAction.actionName = [responseObject valueForKeyPath:@"detect_action.actionName"];
                                 beaconAction.params = (NSArray *) [responseObject valueForKeyPath:@"detect_action.params"];
                                 beaconDetail.action = beaconAction;
                                 
                                 BIBeaconPlacement *beaconPlacement = [[BIBeaconPlacement alloc] init];
                                 beaconPlacement.placementId = (long)[responseObject valueForKeyPath:@"placement.id"];
                                 beaconPlacement.placementName = [responseObject valueForKeyPath:@"placement.name"];
                                 beaconDetail.placement = beaconPlacement;
                                 
                                 BIBeaconLocation *beaconLoaction = [[BIBeaconLocation alloc] init];
                                 beaconLoaction.locationId = (long)[responseObject valueForKeyPath:@"location.id"];
                                 beaconLoaction.locationName = [responseObject valueForKeyPath:@"location.name"];
                                 beaconDetail.location = beaconLoaction;
                                 
                                 return beaconDetail;
                             } else {
                                 [self setBeaconnectErrorAsProtocolError];
                                 NSLog(@"Protocol Error: object=%@", responseObject);
                                 return nil;
                             }
						 } failure:^(NSURLSessionTask *task, NSError *error) {
							 
						 }];
					 } failure:^(NSURLSessionDataTask *task, NSError *error) {
						 // 呼び出し失敗
						 [self handleErrorWithTask:task error:error];
					 }];

}

- (BIBeaconDetail *)beaconDetail {
    return self.beaconDetail;
}

@end
