//
//  BIFetchMobilePhoneRequest.h
//  TGC
//
//  Created by sasajima on 2014/08/07.
//

#import <Foundation/Foundation.h>
#import "BIBeaconnectClientRequest.h"
#import "BIMobilePhone.h"

@interface BIFetchMobilePhoneForCheckRequest : BIBeaconnectClientRequest

- (void)fetchWithSeqId:(NSInteger)seqId mpid:(NSUUID *)mpid;

- (BIMobilePhone *)mobiePhone;

@end
