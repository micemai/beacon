//
//  BIFetchChallengeFromServer
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIUploadAccessPointLogsWithDetectedAt.h"


@implementation BIUploadAccessPointLogsWithDetectedAt

- (void)uploadAccessPointLogsWithDetectedAt:(NSDate *)detectedAt
                           accessDeviceType:(NSString *)accessDeviceType
                                       mpid:(NSString *)mpid
                                 macAddress:(NSString *)macAddress
                                       uuid:(NSString *)uuid
                                        bid:(NSInteger )bid
                                      major:(NSInteger)major
                                      minor:(NSInteger)minor
                                       rssi:(int)rssi
                                   batteryV:(long)batteryV
                                    txPower:(long)txPower
                                temperature:(int)temperature
                                   humidity:(int)humidity
                                     accelX:(int)accelX
                                     accelY:(int)accelY
                                     accelZ:(int)accelZ
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/access_point_logs"];
	NSLog(@"%@", URL);
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *dateString = [formatter stringFromDate:detectedAt];
 	NSDictionary *params = @{
                             @"detected_at" : dateString,
                             @"access_device_type" : accessDeviceType,
                             @"mpid" : mpid,
                             @"mac" : macAddress ? macAddress : @"",
                             @"uuid" : uuid ? uuid : @"",
                             @"bid" : [NSString stringWithFormat:@"%04x%04x", major,minor],
                             @"major" : [NSString stringWithFormat:@"%ld", (long)major],
                             @"minor" : [NSString stringWithFormat:@"%ld", (long)minor],
                             @"rssi" : [NSString stringWithFormat:@"%d", rssi],
                             @"remaining_battery" : [NSString stringWithFormat:@"%ld", batteryV],
                             @"tx_power" : [NSString stringWithFormat:@"%ld", txPower],
                             @"temperature" : [NSString stringWithFormat:@"%d", temperature],
                             @"humidity" : [NSString stringWithFormat:@"%d", humidity],
                             @"accel_x" : [NSString stringWithFormat:@"%d", accelX],
                             @"accel_y" : [NSString stringWithFormat:@"%d", accelY],
                             @"accel_z" : [NSString stringWithFormat:@"%d", accelZ],
  							 };
    
	self.task = [manager POST:URL.absoluteString parameters:params
					 success:^(NSURLSessionDataTask *task, id responseObject) {
						 [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
							 // NSLog(@"%@", responseObject);
                             return responseObject;
						 } failure:^(NSURLSessionTask *task, NSError *error) {
							 
						 }];
					 } failure:^(NSURLSessionDataTask *task, NSError *error) {
						 // 呼び出し失敗
						 [self handleErrorWithTask:task error:error];
					 }];

}

@end
