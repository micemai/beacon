//
//  BIBeaconnectClientRequest.h
//  Beacon
//
//  Created by Toshiki Tsubouchi on 8/2/14.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

//@class BIMasterBeaconnectClient;

@interface BIBeaconnectClientRequest : NSObject

@property (strong, nonatomic) NSURL *baseURL;

@property (strong, nonatomic) NSURLSessionDataTask *task;

@property (assign, nonatomic) NSInteger HTTPStatus;
@property (strong, nonatomic) NSString *beaconnectStatus;
@property (assign, nonatomic) NSInteger beaconnectResultCode;
@property (assign, nonatomic) NSInteger location;
@property (strong, nonatomic) id result;
@property (strong, nonatomic) NSError *error;

@property (copy, nonatomic) void(^successProc)(BIBeaconnectClientRequest *beaconnectClientRequest, id resultObject);
@property (copy, nonatomic) void(^failureProc)(BIBeaconnectClientRequest *beaconnectClientRequest, NSError *error);

- (NSURL *)APIURLWithAPIPath:(NSString *)path;
- (NSURL *)APIURLWithAPIPath:(NSString *)path parameters:(NSDictionary *)parameters;
- (NSMutableURLRequest *)URLRequestWithAPIPath:(NSString *)path;

- (BOOL)isBeaconnectErrorResponse:(NSURLResponse *)response;
- (void)setBeaconnectErrorObjectFromResponseObject:(id)responseObject;
- (void)setBeaconnectErrorAsProtocolError;

- (void)parseResponseWithTask:(NSURLSessionTask *)task responseObject:(id)responseObject
					  success:(id (^)(NSURLSessionTask *task, id responseObject))successProc
					  failure:(void (^)(NSURLSessionTask *task, NSError *error))failureProc;
- (void)handleErrorWithTask:task error:error;

@end
