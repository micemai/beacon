//
//  BIFetchChallengeFromServer
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIBeaconnectClientRequest.h"

@interface BIUpdatePushTokenForService : BIBeaconnectClientRequest

- (void)updatePushTokenForService:(NSString *)serviceId
                          mpSeqId:(long)mpSeqId
                        pushToken:(NSString *) pushToken;

@end
