//
//  BISubscribeToService.h
//  TGC
//
//  Created by sasajima on 2014/08/07.
//

#import <Foundation/Foundation.h>
#import "BIBeaconnectClientRequest.h"

@interface BISubscribeToServiceRequest : BIBeaconnectClientRequest

- (void)subscribeToServiceWithSeqId:(NSInteger)seqId
                                   mpid:(NSUUID *)mpid
                              pushToken:(NSString *)pushToken;

- (NSString *)location;
@end
