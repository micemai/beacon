//
//  BIFetchMobilePhoneRequest.m
//  TGC
//
//  Created by sasajima on 2014/08/07.
//

#import "BIFetchMobilePhoneForCheckRequest.h"

@implementation BIFetchMobilePhoneForCheckRequest

- (void)fetchWithSeqId:(NSInteger)seqId mpid:(NSUUID *)mpid
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/services/:seq_id/mobile_phones/mpid/:mpid"
                              parameters:@{ @"seq_id" : [NSString stringWithFormat:@"%d", seqId],
                                            @"mpid" : [[mpid UUIDString] lowercaseString]
                                            }];
	NSLog (@"%@", URL);
	self.task = [manager GET:URL.absoluteString parameters:nil
                     success:^(NSURLSessionDataTask *task, id responseObject) {
                         [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
                             // NSLog(@"%@", responseObject);
                             if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                 
                                 BIMobilePhone *mp = [[BIMobilePhone alloc] init];
                                 mp.seqId = [responseObject[@"id"] integerValue];
                                 mp.mpid = responseObject[@"mpid"];
                                 mp.osType = responseObject[@"os_type"];
                                 mp.pushToken = responseObject[@"push_token"];
                                 return mp;
                             } else {
                                 [self setBeaconnectErrorAsProtocolError];
                                 NSLog(@"Protocol Error: object=%@", responseObject);
                                 return nil;
                             }
                         } failure:^(NSURLSessionTask *task, NSError *error) {
                             
                         }];
                     } failure:^(NSURLSessionDataTask *task, NSError *error) {
                         // 呼び出し失敗
                         [self handleErrorWithTask:task error:error];
                     }];
}

- (BIMobilePhone *)mobiePhone {
	return self.result;
}


@end
