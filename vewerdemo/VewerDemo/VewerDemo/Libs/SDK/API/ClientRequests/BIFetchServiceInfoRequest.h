//
//  BIFetchServiceInfoRequest.h
//  TGC
//
//  Created by sasajima on 2014/08/07.
//

#import <Foundation/Foundation.h>

#import "BIBeaconnectClientRequest.h"
#import "BIServiceInfo.h"

@interface BIFetchServiceInfoRequest : BIBeaconnectClientRequest

- (void)fetchWithSid:(NSString *)sid;

- (BIServiceInfo *)serviceInfo;

@end
