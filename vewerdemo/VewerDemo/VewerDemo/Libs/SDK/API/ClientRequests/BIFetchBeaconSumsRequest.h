//
//  BIFetchBeaconsRequest.h
//  TGC
//
//  Created by sasajima on 2014/08/08.
//

#import <Foundation/Foundation.h>
#import "BIBeaconnectClientRequest.h"
#import "BIBeaconSummary.h"

@interface BIFetchBeaconSumsRequest : BIBeaconnectClientRequest

- (void)fetchFromService:(NSInteger)seqId;

- (NSArray *)beaconSums;

@end
