//
//  BIFetchAPIKeyRequest.m
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIFetchAccessTokenFromServer.h"


@implementation BIFetchAccessTokenFromServer

- (void)fetchAccessTokenFromServer:(NSString *)serviceId
                              mpid:(NSUUID *)mpid
                        credential:(NSString *)credential
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/auth/service/access_tokens"];
	NSLog(@"%@", URL);
    
    NSDictionary *params = @{
                             @"auth_id" :  serviceId,
                             @"end_point_id" : [mpid UUIDString],
                             @"response" : credential
                             };
    
	self.task = [manager POST:URL.absoluteString parameters:params
					 success:^(NSURLSessionDataTask *task, id responseObject) {
						 [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
							 // NSLog(@"%@", responseObject);
							 if ([responseObject isKindOfClass:[NSDictionary class]]) {
								 return responseObject[@"token"];
							 } else {
								 [self setBeaconnectErrorAsProtocolError];
								 NSLog(@"Protocol Error: object=%@", responseObject);
								 return nil;
							 }
						 } failure:^(NSURLSessionTask *task, NSError *error) {
							 
						 }];
					 } failure:^(NSURLSessionDataTask *task, NSError *error) {
						 // 呼び出し失敗
						 [self handleErrorWithTask:task error:error];
					 }];

}

@end
