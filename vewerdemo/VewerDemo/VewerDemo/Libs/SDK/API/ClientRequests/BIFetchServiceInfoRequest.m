//
//  BIFetchServiceInfoRequest.m
//  TGC
//
//  Created by sasajima on 2014/08/07.
//

#import "BIFetchServiceInfoRequest.h"

@implementation BIFetchServiceInfoRequest

- (void)fetchWithSid:(NSString *)sid
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/services/sid/:sid" parameters:@{ @"sid" : sid }];
	NSLog (@"%@", URL);
	self.task = [manager GET:URL.absoluteString parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
                 // NSLog(@"%@", responseObject);
                 if ([responseObject isKindOfClass:[NSDictionary class]]) {
                     BIServiceInfo *serviceInfo = [[BIServiceInfo alloc] init];
                     // TODO サービスを初期化する。
                     serviceInfo.seqId = [responseObject[@"id"] integerValue];
                     serviceInfo.sid = [responseObject[@"sid"] integerValue]; // FIXME 本当は文字列型
                     serviceInfo.uuid = responseObject[@"uuid"];
                     //service.uuid = [[NSUUID alloc] initWithUUIDString:responseObject[@"uuid"]];
                     serviceInfo.version = responseObject[@"version"];
                     return serviceInfo;
                 } else {
                     [self setBeaconnectErrorAsProtocolError];
                     NSLog(@"Protocol Error: object=%@", responseObject);
                     return nil;
                 }
             } failure:^(NSURLSessionTask *task, NSError *error) {
                 
             }];
         } failure:^(NSURLSessionDataTask *task, NSError *error) {
             // 呼び出し失敗
             [self handleErrorWithTask:task error:error];
         }];
}

- (BIServiceInfo *)serviceInfo {
	return self.result;
}

@end
