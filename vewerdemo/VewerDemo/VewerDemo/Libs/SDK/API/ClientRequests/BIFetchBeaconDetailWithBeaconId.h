//
//  BIFetchChallengeFromServer
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIBeaconnectClientRequest.h"
#import "BIBeaconDetail.h"

@interface BIFetchBeaconDetailWithBeaconId : BIBeaconnectClientRequest

- (void)fetchBeaconDetailWithBeaconId:(NSInteger)beaconId;

- (BIBeaconDetail *) beaconDetail;

@end
