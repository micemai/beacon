//
//  BIFetchChallengeFromServer
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIBeaconnectClientRequest.h"

@interface BIUploadAccessPointLogsWithDetectedAt : BIBeaconnectClientRequest

- (void)uploadAccessPointLogsWithDetectedAt:(NSDate *)detectedAt
                           accessDeviceType:(NSString *)accessDeviceType
                                       mpid:(NSString *)mpid
                                 macAddress:(NSString *)macAddress
                                       uuid:(NSString *)uuid
                                        bid:(NSInteger )bid
                                      major:(NSInteger)major
                                      minor:(NSInteger)minor
                                       rssi:(int)rssi
                                   batteryV:(long)batteryV
                                    txPower:(long)txPower
                                temperature:(int)temperature
                                   humidity:(int)humidity
                                     accelX:(int)accelX
                                     accelY:(int)accelY
                                     accelZ:(int)accelZ;

@end
