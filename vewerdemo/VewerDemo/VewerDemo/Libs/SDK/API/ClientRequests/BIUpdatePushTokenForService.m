//
//  BIFetchChallengeFromServer
//  Beacon
//
//  Created by Y.Sasajima on 8/8/14.
//

#import "BIUpdatePushTokenForService.h"


@implementation BIUpdatePushTokenForService

- (void)updatePushTokenForService:(NSString *)serviceId
                          mpSeqId:(long)mpSeqId
                        pushToken:(NSString *) pushToken
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/services/:service_id/mobile_phones/:id" parameters:@{ @"service_id" : serviceId, @"id" : [NSString stringWithFormat:@"%ld", mpSeqId] }];
	NSLog(@"%@", URL);
    
	NSDictionary *params = @{
                             @"push_token" : pushToken
							 };
    
	self.task = [manager PUT:URL.absoluteString parameters:params
					 success:^(NSURLSessionDataTask *task, id responseObject) {
						 [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
							 // NSLog(@"%@", responseObject);
                             return responseObject;
						 } failure:^(NSURLSessionTask *task, NSError *error) {
							 
						 }];
					 } failure:^(NSURLSessionDataTask *task, NSError *error) {
						 // 呼び出し失敗
						 [self handleErrorWithTask:task error:error];
					 }];

}

@end
