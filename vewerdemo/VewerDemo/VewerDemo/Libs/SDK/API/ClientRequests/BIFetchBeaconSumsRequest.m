//
//  BIFetchBeaconsRequest.m
//  TGC
//
//  Created by sasajima on 2014/08/08.
//

#import "BIFetchBeaconSumsRequest.h"

@implementation BIFetchBeaconSumsRequest

- (void)fetchFromService:(NSInteger)seqId
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSURL *URL = [self APIURLWithAPIPath:@"api/0.1/services/:seq_id/beacons" parameters:@{ @"seq_id" : [NSString stringWithFormat:@"%ld", (long)seqId] }];
	NSLog (@"%@", URL);
	self.task = [manager GET:URL.absoluteString parameters:nil
                     success:^(NSURLSessionDataTask *task, id responseObject) {
                         [self parseResponseWithTask:task responseObject:responseObject success:^id(NSURLSessionTask *task, id responseObject) {
                             // NSLog(@"%@", responseObject);
                             if ([responseObject isKindOfClass:[NSArray class]]) {
                                 NSMutableArray *_beaconSums = [[NSMutableArray alloc] init];
                                 for (NSDictionary *dict in responseObject) {
                                     BIBeaconSummary *beaconSum = [[BIBeaconSummary alloc] init];
                                     beaconSum.seqId =  [[dict objectForKey:@"id"] longValue];
                                     NSDictionary *innerDict = [dict objectForKey:@"beacon_summary"] ;
                                     beaconSum.serviceId = seqId;
                                     
                                     
                                     // FIXME
                                     beaconSum.bid = [[innerDict objectForKey:@"bid"] integerValue];
                                     
                                     NSString *bidString = [[NSString alloc] initWithFormat:@"%08ld",(long)beaconSum.bid];
                                     NSString *majorString = [bidString substringWithRange:NSMakeRange(0, 4)];
                                     NSString *minorString = [bidString substringWithRange:NSMakeRange(4, 4)];
                                     
                                     unsigned int outMajor, outMinor;
                                     NSScanner* scanner = [NSScanner scannerWithString:majorString];
                                     [scanner scanHexInt:&outMajor];
                                     
                                     scanner = [NSScanner scannerWithString:minorString];
                                     [scanner scanHexInt:&outMinor];

                                     beaconSum.major = [NSNumber numberWithInteger:outMajor];
                                     beaconSum.minor = [NSNumber numberWithInteger:outMinor];
                                     
                                     beaconSum.threshold = [[innerDict objectForKey:@"min_rssi"] integerValue];
                                     beaconSum.exclusiveTime = [[innerDict objectForKey:@"exclusive_time"] integerValue];
                                     
                                     
                                     
                                     [_beaconSums addObject:beaconSum];
                                 }
                                 return _beaconSums;
                             } else {
                                 [self setBeaconnectErrorAsProtocolError];
                                 NSLog(@"Protocol Error: object=%@", responseObject);
                                 return nil;
                             }
                         } failure:^(NSURLSessionTask *task, NSError *error) {
                             
                         }];
                     } failure:^(NSURLSessionDataTask *task, NSError *error) {
                         // 呼び出し失敗
                         [self handleErrorWithTask:task error:error];
                     }];
    
}

- (NSArray *)beaconSums;
{
    return self.result;
}



@end
