//
//  BIFetchMobilePhoneRequest.h
//  TGC
//
//  Created by sasajima on 2014/08/07.
//

#import <Foundation/Foundation.h>
#import "BIBeaconnectClientRequest.h"
#import "BIMobilePhone.h"

@interface BIFetchMobilePhoneRequest : BIBeaconnectClientRequest

- (void)fetchWithLocation:(NSString *)location;

- (BIMobilePhone *)mobiePhone;

@end
