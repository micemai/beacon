//
//  Constant.h
//  Golfans
//
//  Created by Nguyen Phuong Mai on 4/1/14.
//  Copyright (c) 2014 Nguyen Phuong Mai. All rights reserved.
//

#ifndef Golfans_Constant_h
#define Golfans_Constant_h

//Define all key here
#define kIphoneWidth 320
#define kIphone5Height 568
#define kIphone4Height 480

#if DEBUG
//static NSString * const BASE_URL = @"http://beacon.myu-on.net/";
#define BASE_URL @"http://beacon.myu-on.net/"
#else
//static NSString * const BASE_URL = @"http://172.16.1.83:3000/";
//static NSString * const BASE_URL = @"http://172.16.1.98:3000/";
#define BASE_URL @"http://beacon.myu-on.net/"
#endif

#endif
