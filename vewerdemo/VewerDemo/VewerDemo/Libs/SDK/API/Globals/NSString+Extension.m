//
//  NSString+Extension.m
//  Beacon
//
//  Created by Toshiki Tsubouchi on 8/4/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (Extension)

+ (instancetype)stringWithInteger:(NSInteger)value
{
	return [NSString stringWithFormat:@"%ld", (long)value];
}

@end
