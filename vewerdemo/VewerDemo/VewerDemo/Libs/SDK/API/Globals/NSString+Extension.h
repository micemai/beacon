//
//  NSString+Extension.h
//  Beacon
//
//  Created by Toshiki Tsubouchi on 8/4/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

+ (instancetype)stringWithInteger:(NSInteger)value;

@end
