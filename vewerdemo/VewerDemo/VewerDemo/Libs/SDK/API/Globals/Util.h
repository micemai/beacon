//
//  Util.h
//  Beacon
//
//  Created by Nguyen Phuong Mai on 7/21/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SESSION_VERSION @"21072014"
#define SESSION_KEY @"SESSION_DATA"
#define COLOR_DEFAULT RGB(104, 194, 233)

@interface Util : NSObject

@property (nonatomic, assign) BOOL isAuthenticated;

+ (Util *)sharedInstance;
- (void)save;
- (void)getData;
- (void)remove;

@end
