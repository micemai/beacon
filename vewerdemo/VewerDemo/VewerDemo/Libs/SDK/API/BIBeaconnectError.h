//
//  BIBeaconnectError.h
//  Beacon
//
//  Created by Toshiki Tsubouchi on 8/2/14.
//

#import <Foundation/Foundation.h>

@interface BIBeaconnectError : NSError

@end
