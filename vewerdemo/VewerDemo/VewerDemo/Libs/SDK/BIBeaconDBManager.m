//
//  BIBeaconDBManager.m
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/25.
//
//

#import "BIBeaconDBManager.h"
#import "FMDB/FMDatabase.h"
#import "FMDB/FMResultSet.h"


static NSString * const DB_FILE_NAME = @"Beaconnect.db";

// service info
static NSString * const SERVICES_SQL_CREATE = @"CREATE TABLE IF NOT EXISTS services (id INTEGER, sid INTEGER, uuid TEXT,version TEXT);";
static NSString * const SERVICES_SQL_INSERT = @"INSERT INTO services (id, sid, uuid, version) VALUES (?, ?, ?, ?);";
static NSString * const SERVICES_SQL_UPDATE = @"UPDATE services SET uuid = ?, version = ? WHERE sid = ?;";
static NSString * const SERVICES_SQL_SELECT = @"SELECT id, sid, uuid, version FROM services WHERE sid = ?;";
static NSString * const SERVICES_SQL_SELECT_ID_WITH_UUID = @"SELECT id FROM services WHERE uuid = ? COLLATE NOCASE;";

// mobilephone
static NSString * const MOBILEPHONE_SQL_CREATE = @"CREATE TABLE IF NOT EXISTS mobilephone (id INTEGER, mpid TEXT, service_id INTEGER, os_type TEXT,push_token TEXT);";
static NSString * const MOBILEPHONE_SQL_INSERT = @"INSERT INTO mobilephone (id, service_id, mpid, os_type, push_token) VALUES (?, ?, ?, ?, ?);";
static NSString * const MOBILEPHONE_SQL_UPDATE = @"UPDATE mobilephone SET push_token = ? WHERE id = ?;";
static NSString * const MOBILEPHONE_SQL_SELECT_MPID = @"SELECT id, service_id, mpid, os_type, push_token FROM mobilephone WHERE mpid = ? AND service_id = ?;";

// beacon_summarys
static NSString * const BEACONS_SQL_CREATE = @"CREATE TABLE IF NOT EXISTS beacon_summarys (id INTEGER, service_id INTEGER, bid INTEGER, min_rssi INTEGER, exclusive_time INTEGER);";
static NSString * const BEACONS_SQL_INSERT = @"INSERT INTO beacon_summarys (id, service_id, bid, min_rssi, exclusive_time) VALUES (?, ?, ?, ?, ?);";
static NSString * const BEACONS_SQL_DELETE = @"DELETE FROM beacon_summarys WHERE service_id = ?;";
static NSString * const BEACONS_SQL_SELECT_LIST = @"SELECT id, service_id, bid, min_rssi, exclusive_time FROM beacon_summarys WHERE service_id = ?;";


// :detected_at, :access_device_type :mac, :uuid, :bid,:major, :minor, :rssi,
// :remaining_battery, :tx_power, :temperature, :humidity, :accel_x, :accel_y, :accel_z
// log
static NSString * const LOG_SQL_CREATE = @"CREATE TABLE IF NOT EXISTS detected_log (id INTEGER PRIMARY KEY AUTOINCREMENT, detected_at DATETIME, access_device_type TEXT, mpid TEXT, mac TEXT, uuid TEXT, bid INTEGER, major INTEGER, minor INTEGER, rssi INTEGER, remaining_battery INTEGER, tx_power INTEGER, temperature INTEGER, humidity INTEGER, gyroX INTEGER, gyroY INTEGER, gyroZ INTEGER, is_send INTEGER);";
static NSString * const LOG_SQL_INSERT = @"INSERT INTO detected_log (detected_at, access_device_type, mpid, mac, uuid, bid, major, minor, rssi, remaining_battery, tx_power, temperature, humidity, gyroX, gyroY, gyroZ, is_send) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0);";
static NSString * const LOG_SQL_SELECT_LIST = @"SELECT id, detected_at, access_device_type, mpid, mac, uuid, bid, major, minor, rssi, remaining_battery, tx_power, temperature, humidity, gyroX, gyroY, gyroZ from detected_log where is_send = 0;";
static NSString * const LOG_SQL_UPDATE_IS_SEND = @"UPDATE detected_log SET is_send = 1 WHERE uuid = ? AND bid = ?;";
static NSString * const LOG_SQL_DELETE = @"DELETE FROM detected_log WHERE is_send = 1;";
static NSString * const LOG_SQL_DELETE_WITH_ID = @"DELETE FROM detected_log WHERE id = ?;";

// tokens
static NSString * const TOKENS_SQL_CREATE = @"CREATE TABLE IF NOT EXISTS tokens (sid INTEGER, mpid INTEGER, api_key TEXT, access_token TEXT);";
static NSString * const TOKENS_SQL_INSERT = @"INSERT INTO tokens (sid, mpid, api_key, access_token) VALUES (?, ?, ?, ?);";
static NSString * const TOKENS_SQL_UPDATE = @"UPDATE tokens SET access_token = ? WHERE sid = ? AND mpid = ?;";


@interface BIBeaconDBManager()
@property (strong, nonatomic) NSString *dbPath;
@end

@implementation BIBeaconDBManager

+ (instancetype)sharedInstance
{
    static BIBeaconDBManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[BIBeaconDBManager alloc] init];
    });
    return instance;
}

- (FMDatabaseQueue *)dbQueue
{
    if (!_dbQueue) {
        _dbQueue = [FMDatabaseQueue databaseQueueWithPath:[BIBeaconDBManager sharedInstance].dbPath];
    };
    return _dbQueue;
}

- (instancetype)init
{
	self = [super init];
	if( self )
	{
		FMDatabase* db = [self getConnection];
		[db open];
		[db executeUpdate:MOBILEPHONE_SQL_CREATE];
		[db executeUpdate:SERVICES_SQL_CREATE];
		[db executeUpdate:BEACONS_SQL_CREATE];
		[db executeUpdate:LOG_SQL_CREATE];
		[db executeUpdate:TOKENS_SQL_CREATE];
		[db close];
	}
    
	return self;
}

/**
* データベースを取得します。
*
* @return データベース。
*/
- (FMDatabase *)getConnection
{
	if( self.dbPath == nil )
	{
		self.dbPath =  [BIBeaconDBManager getDbFilePath];
	}
	
	return [FMDatabase databaseWithPath:self.dbPath];
}


/**
 * データベース ファイルのパスを取得します。
 */
+ (NSString*)getDbFilePath
{
	NSArray*  paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
	NSString* dir   = [paths objectAtIndex:0];
	
	return [dir stringByAppendingPathComponent:DB_FILE_NAME];
}


- (BIServiceInfo *)addServiceInfo:(BIServiceInfo *)serviceInfo
{
	FMDatabase* db = [self getConnection];
	[db open];
	if( ![db executeUpdate:SERVICES_SQL_INSERT,
         [NSNumber numberWithInteger:serviceInfo.seqId], [NSNumber numberWithInteger:serviceInfo.sid], serviceInfo.uuid, serviceInfo.version] ) {
		serviceInfo = nil;
	}
	[db close];
    
	return serviceInfo;
}

- (BIServiceInfo *)updateServiceInfo:(BIServiceInfo *)serviceInfo
{
	FMDatabase* db = [self getConnection];
	[db open];
	[db setShouldCacheStatements:YES];
	if( ![db executeUpdate:SERVICES_SQL_UPDATE,
          serviceInfo.uuid, serviceInfo.version, [NSNumber numberWithInteger:serviceInfo.sid]] ) {
		serviceInfo = nil;
	}
	[db close];
    
	return serviceInfo;
}

- (BIServiceInfo *)referServiceInWithSid:(int)sid
{
	FMDatabase* db = [self getConnection];
	[db open];
    
    FMResultSet *rs = [db executeQuery:SERVICES_SQL_SELECT, [NSNumber numberWithInt:sid]];
    
    BIServiceInfo* result = nil;
    if ([rs next]) {
        result = [[BIServiceInfo alloc] init];
        result.seqId = [rs intForColumn:@"id"];
        result.sid = [rs intForColumn:@"sid"];
        result.uuid = [rs stringForColumn:@"uuid"];
        result.version = [rs stringForColumn:@"version"];
    }
    
	[db close];
    
	return result;
}

- (NSInteger)referServiceIdWithUuid:(NSUUID *)uuid
{
   	FMDatabase* db = [self getConnection];
	[db open];
    FMResultSet* rs = [db executeQuery:SERVICES_SQL_SELECT_ID_WITH_UUID, [uuid UUIDString]];
    
    NSInteger result = 0;
    if ([rs next]) {
        result = [rs intForColumn:@"id"];
    }
    
	[db close];
    
	return result;
 
}

- (BIMobilePhone *)addMobilePhone:(BIMobilePhone *)mobile
{
	FMDatabase* db = [self getConnection];
	[db open];
	[db setShouldCacheStatements:YES];
	if( ![db executeUpdate:MOBILEPHONE_SQL_INSERT,
          [NSNumber numberWithInteger:mobile.seqId], [NSNumber numberWithInteger:mobile.serviceId], mobile.mpid, mobile.osType, mobile.pushToken] ) {
		mobile = nil;
	}
	[db close];
    
	return mobile;
}

- (BIMobilePhone *)updateMobilePhone:(BIMobilePhone *)mobile
{
    FMDatabase* db = [self getConnection];
	[db open];
	[db setShouldCacheStatements:YES];
	if( ![db executeUpdate:MOBILEPHONE_SQL_UPDATE,
          mobile.pushToken, [NSNumber numberWithInteger:mobile.seqId]] ) {
		mobile = nil;
	}
	[db close];
    
	return mobile;
}

- (BIMobilePhone *)referMobilePhoneWithMpid:(NSUUID *) mpid serviceId:(int)serviceId
{
    FMDatabase* db = [self getConnection];
	[db open];
    FMResultSet* rs = [db executeQuery:MOBILEPHONE_SQL_SELECT_MPID, mpid, [NSNumber numberWithInt:serviceId]];
    
    BIMobilePhone* result = nil;
    if ([rs next]) {
        result = [[BIMobilePhone alloc] init];
        result.seqId = [rs intForColumn:@"id"];
        result.serviceId = [rs intForColumn:@"service_id"];
        result.mpid = [rs stringForColumn:@"mpid"];
        result.osType = [rs stringForColumn:@"os_type"];
        result.pushToken = [rs stringForColumn:@"push_token"];
    }
    
	[db close];
    
	return result;
}

- (BIBeaconSummary *)addBeaconSummary:(BIBeaconSummary *)summary
{
 	FMDatabase* db = [self getConnection];
	[db open];
	[db setShouldCacheStatements:YES];
	if( ![db executeUpdate:BEACONS_SQL_INSERT,
          [NSNumber numberWithInteger:summary.seqId], [NSNumber numberWithInteger:summary.serviceId], [NSNumber numberWithInteger:summary.bid], [NSNumber numberWithInteger:summary.threshold], [NSNumber numberWithInteger:summary.exclusiveTime]] ) {
		summary = nil;
	}
	[db close];
    
	return summary;
}

- (BOOL)deleteBeaconSummaryWithServiceId:(int) serviceId
{
    FMDatabase* db = [self getConnection];
	[db open];
    
	BOOL isSucceeded = [db executeUpdate:BEACONS_SQL_DELETE, [NSNumber numberWithInteger:serviceId]];
	
	[db close];
    
	return isSucceeded;
}

- (NSArray *)referBeaconSummariesWithServiceId:(int) serviceId
{
    FMDatabase* db = [self getConnection];
	[db open];
    FMResultSet* rs = [db executeQuery:BEACONS_SQL_SELECT_LIST, [NSNumber numberWithInteger:serviceId]];
    
    NSMutableArray* result = [NSMutableArray array];
    while ([rs next]) {
        BIBeaconSummary* record = [[BIBeaconSummary alloc] init];
        record.seqId = [rs intForColumn:@"id"];
        record.serviceId = [rs intForColumn:@"service_id"];
        record.bid = [rs intForColumn:@"bid"];
        record.threshold = [rs intForColumn:@"min_rssi"];
        record.exclusiveTime = [rs intForColumn:@"exclusive_time"];
        
        [result addObject:record];
    }
    
	[db close];
    
	return result;
}

- (BIBeaconLog *)addBeaconLog:(BIBeaconLog*)log
{
 	FMDatabase* db = [self getConnection];
	[db open];
	[db setShouldCacheStatements:YES];
	if( ![db executeUpdate:LOG_SQL_INSERT,
          log.detectedDate, log.accessDeviceType,log.mpid, log.macAddress, log.uuid,
          [NSNumber numberWithInteger:log.bid],
          [NSNumber numberWithInteger:log.major],
          [NSNumber numberWithInteger:log.minor],
          [NSNumber numberWithInteger:log.rssi],
          [NSNumber numberWithInteger:log.batteryVoltage],
          [NSNumber numberWithInteger:log.txPower],
          [NSNumber numberWithInteger:log.temperature],
          [NSNumber numberWithInteger:log.humidity],
          [NSNumber numberWithInteger:log.gyroX],
          [NSNumber numberWithInteger:log.gyroY],
          [NSNumber numberWithInteger:log.gyroZ],
          @0] ) {
		log = nil;
	}
	[db close];
    
	return log;
}

- (BIBeaconLog *)updateBeaconLog:(BIBeaconLog *)log
{
   	FMDatabase* db = [self getConnection];
	[db open];
	[db setShouldCacheStatements:YES];
	if( ![db executeUpdate:LOG_SQL_UPDATE_IS_SEND,
          log.uuid, log.bid] ) {
		log = nil;
	}
	[db close];
    
	return log;
}

- (BOOL)deleteBeaconLogIsSent
{
 	FMDatabase* db = [self getConnection];
	[db open];
    
	BOOL isSucceeded = [db executeUpdate:LOG_SQL_DELETE];
	
	[db close];
    
	return isSucceeded;
}
- (NSArray *)fetechBeaconLogs
{
    // :detected_at, :access_device_type :mac, :uuid, :bid,:major, :minor, :rssi,
    // :remaining_battery, :tx_power, :temperature, :humidity, :accel_x, :accel_y, :accel_z
    // log
    
    FMDatabase* db = [self getConnection];
	[db open];
    FMResultSet* rs = [db executeQuery:LOG_SQL_SELECT_LIST];
    
    NSMutableArray* result = [NSMutableArray array];
    while ([rs next]) {
        BIBeaconLog* log = [[BIBeaconLog alloc] init];
        log.seqId = [rs intForColumn:@"id"];
        log.detectedDate = [rs dateForColumn:@"detected_at"];
        log.accessDeviceType = [rs stringForColumn:@"access_device_type"];
        log.mpid = [rs stringForColumn:@"mpid"];
        log.macAddress = [rs stringForColumn:@"mac"];
        log.uuid = [rs stringForColumn:@"uuid"];
        log.bid = [rs intForColumn:@"bid"];
        log.major = [rs intForColumn:@"major"];
        log.minor = [rs intForColumn:@"minor"];
        log.rssi = [rs intForColumn:@"rssi"];
        log.batteryVoltage = [rs intForColumn:@"remaining_battery"];
        log.txPower = [rs intForColumn:@"tx_power"];
        log.txPower =[rs intForColumn:@"temperature"];
        log.humidity =[rs intForColumn:@"humidity"];
        log.gyroX =[rs intForColumn:@"gyroX"];
        log.gyroY =[rs intForColumn:@"gyroY"];
        log.gyroZ =[rs intForColumn:@"gyroZ"];
        
        [result addObject:log];
    }
    
	[db close];
    
	return result;
}

- (BOOL)deleteBeaconLogWithSeqId:(int)seqId
{
	FMDatabase* db = [self getConnection];
	[db open];
    
	BOOL isSucceeded = [db executeUpdate:LOG_SQL_DELETE_WITH_ID, [NSNumber numberWithInteger:seqId]];
	
	[db close];
    
	return isSucceeded;
}


- (BIToken *)addToaken:(BIToken *)token
{
 	FMDatabase* db = [self getConnection];
	[db open];
	[db setShouldCacheStatements:YES];
	if( ![db executeUpdate:TOKENS_SQL_INSERT,
          token.sid, token.mpid, token.apiKey, token.accessToken] ) {
		token = nil;
	}
	[db close];
    
	return token;
}

- (BIToken *)updateToken:(BIToken *)token
{
 	FMDatabase* db = [self getConnection];
	[db open];
	[db setShouldCacheStatements:YES];
	if( ![db executeUpdate:TOKENS_SQL_UPDATE,
          token.accessToken, token.sid, token.mpid] ) {
		token = nil;
	}
	[db close];
    
	return token;
}

@end
