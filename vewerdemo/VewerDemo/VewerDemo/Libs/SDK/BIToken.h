//
//  BIToken.h
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/25.
//
//

#import <Foundation/Foundation.h>

@interface BIToken : NSObject

@property (strong, nonatomic) NSString *sid;
@property (strong, nonatomic) NSString *mpid;
@property (strong, nonatomic) NSString *apiKey;
@property (strong, nonatomic) NSString *accessToken;

@end
