//
//  BIBeaconData.h
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/25.
//
//

#import <Foundation/Foundation.h>
#import "BIBeacon.h"

@class BIBeaconSummary;

@interface BIBeaconData : NSObject

@property (strong, nonatomic) NSString *identifier;
@property (assign, nonatomic) NSInteger lastRssi;

@property (strong, nonatomic) BIBeacon *beacon;
@property (strong, nonatomic) BIBeaconSummary *summary;

@end