//
//  BIBeaconDetail.h
//  Beaconnect
//
//  Created by zhu on 2014/07/24.
//  Copyright (c) 2014年 zhu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BIBeaconAction.h"

@class BIBeaconPlacement, BIBeaconLocation;

// ビーコン詳細
@interface BIBeaconDetail : NSObject
@property (assign, nonatomic) long beaconId;
@property (strong, nonatomic) NSString *macAddress;
@property (assign, nonatomic) NSInteger bid;
@property (assign, nonatomic) NSInteger major;
@property (assign, nonatomic) NSInteger minor;
@property (strong, nonatomic) BIBeaconAction *action;
@property (strong, nonatomic) BIBeaconPlacement *placement;
@property (strong, nonatomic) BIBeaconLocation *location;

@end

// 配置情報
@interface BIBeaconPlacement : NSObject
@property (assign, nonatomic) long placementId;
@property (strong, nonatomic) NSString *placementName;
@end

// ロケーション情報
@interface BIBeaconLocation : NSObject
@property (assign, nonatomic) long locationId;
@property (strong, nonatomic) NSString *locationName;
@end
