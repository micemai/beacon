//
//  BIDriversManager.m
//  Beaconnect-ios-sdk
//
//  Created by Toshiki Tsubouchi on 8/8/14.
//  Copyright (c) 2014 Toshiki Tsubouchi. All rights reserved.
//

#import "BIDriversManager.h"
#import "BIUtils.h"
#import "BIBeaconData.h"
#import "BIBeaconCollection.h"

@interface BIDriversManager ()
@end

@implementation BIDriversManager {
	volatile BOOL _threadRunning;
	NSTimer *_updateTimer;
    NSTimer *_sendbeaconLogTimer;
	dispatch_queue_t _managerQueue;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
        NSAssert([NSThread isMainThread], @"CLLocationManager must be allocated on the main thread.");
		_locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.activityType = CLActivityTypeFitness;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.pausesLocationUpdatesAutomatically = YES;
		_managerQueue = dispatch_queue_create("jp.beaconnect.driversmanager", 0);
	}
	return self;
}

- (void)startMonitoringWithProximityUUID:(NSUUID *)proximityUUID identifier:identifier targetBeacons:(NSSet *)targetBeacons timeout:(NSTimeInterval)timeout
{
	// コレクションリストを更新する。
	_beaconCollection = [[BIBeaconCollection alloc] initWithTargetBeacons:targetBeacons timeout:timeout];

    NSUUID *uuidIdentifier = [NSUUID UUID];
	// ビーコンのモニタリングを開始する。
	_beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID identifier:[uuidIdentifier UUIDString]];
	_beaconRegion.notifyOnEntry = YES;
	_beaconRegion.notifyOnExit = YES;
	_beaconRegion.notifyEntryStateOnDisplay = NO;
	[_locationManager startMonitoringForRegion:_beaconRegion];
	BILog(@"Start driver: %@ (id:%@)", proximityUUID, identifier);

	// 最初から範囲内にいるとイベントが走らないのでここで走らせておく。
    [self runBackgroundTask];

	if (_updateTimer == nil) {
		_updateTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(update:) userInfo:nil repeats:YES];
		[[NSRunLoop mainRunLoop] addTimer:_updateTimer forMode:NSRunLoopCommonModes];
		[_updateTimer fire];
	}
    
	if (_sendbeaconLogTimer == nil) {
		_sendbeaconLogTimer = [NSTimer timerWithTimeInterval:60.0 target:self selector:@selector(sendbeaconLog:) userInfo:nil repeats:YES];
		[[NSRunLoop mainRunLoop] addTimer:_updateTimer forMode:NSRunLoopCommonModes];
		[_sendbeaconLogTimer fire];
	}
    
}

- (void)stopMonitoringWithProximityUUID:(NSUUID *)proximityUUID
{
	// ビーコンのモニタリングを停止する。
	for (CLBeaconRegion *monitoredRegion in [_locationManager monitoredRegions]) {
		if ([monitoredRegion.proximityUUID isEqual:proximityUUID]) {
			[_locationManager stopMonitoringForRegion:monitoredRegion];
            [_locationManager stopRangingBeaconsInRegion:monitoredRegion];
			BILog(@"Stop monitor: %@", proximityUUID);
		}
	}

	if ([_updateTimer isValid]) {
		[_updateTimer invalidate];
	}
	_updateTimer = nil;
}

////////////////////////////////////////////////////////////////////////////////
#pragma mark - CLLocationManagerDelegate
////////////////////////////////////////////////////////////////////////////////

// 領域観測が正常に開始されると呼ばれる
- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    [_locationManager requestStateForRegion:region];
}

// 領域に関する状態を取得する
- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    switch (state) {
        case CLRegionStateInside:
            if ([region isMemberOfClass:[CLBeaconRegion class]] && [CLLocationManager isRangingAvailable]) {
                //[manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
                [self runBackgroundTask];
            }
            break;
        case CLRegionStateOutside:
            break;
        case CLRegionStateUnknown:
            break;
        default:
            break;
    }
}

// 領域に入ると呼ばれる
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
	//dispatch_async(_managerQueue, ^{
		BILog(@"CoreLocation: ビーコン領域に入りました");
		//[_locationManager startMonitoringForRegion:region];
	//});
    
    [self runBackgroundTask];
}

// 領域から出ると呼ばれる
- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    dispatch_async(_managerQueue, ^{
        BILog(@"CoreLocation: ビーコン領域を出ました");
    //   [manager stopMonitoringForRegion:region];
    });

}

// 領域内でビーコンを受信する度に呼ばれる（実機で確認する限りでは約1秒毎）
// ビーコンの状態が変わった時も呼ばれる
- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    
    if (manager != self.locationManager || ![region.proximityUUID.UUIDString isEqualToString:self.beaconRegion.proximityUUID.UUIDString] || ![region.identifier isEqualToString:self.beaconRegion.identifier] ) {
        return ;
    }
    
    if ([beacons count] > 0) {
        [self updateBeacons:beacons];
    }
}

// アプリのロケーションサービスに関するアクセス許可状態に変更があると呼ばれる
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
	dispatch_async(_managerQueue, ^{
	});
}


- (void)updateBeacons:(NSArray *)beacons
{
     dispatch_async(_managerQueue, ^{
    // コレクションを更新する。
         [_beaconCollection updateBeacons:beacons];
         dispatch_sync(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_DETECT_LIST" object:self userInfo:nil];
            });
     });
}

- (void)update:(NSTimer *)timer
{
	dispatch_async(_managerQueue, ^{
		[_beaconCollection update];
        dispatch_sync(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_DETECT_LIST" object:self userInfo:nil];
        });
	});
}

-(void)sendbeaconLog:(NSTimer *)timer
{
    dispatch_async(_managerQueue, ^{
		[_beaconCollection sendbeaconLog];
    });
}



// Background Task を実行する
-(void)runBackgroundTask
{
    UIApplication *application = [UIApplication sharedApplication];
    
    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            // 既に実行済みであれば終了する
            if (bgTask != UIBackgroundTaskInvalid) {
                [application endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    }];
    
    dispatch_async(_managerQueue, ^{
        // 距離観測を開始する
        [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
    });
}


@end
