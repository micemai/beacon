//
//  BIBeaconLog.h
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/25.
//
//

#import <Foundation/Foundation.h>

@interface BIBeaconLog : NSObject

@property (assign, nonatomic) NSInteger seqId;
@property (strong, nonatomic) NSDate *detectedDate;
@property (strong, nonatomic) NSString *accessDeviceType;
@property (strong, nonatomic) NSString *mpid;
@property (strong, nonatomic) NSString *uuid;
@property (assign, nonatomic) NSInteger bid;
@property (strong, nonatomic) NSString *macAddress;
@property (assign, nonatomic) NSInteger major;
@property (assign, nonatomic) NSInteger minor;
@property (assign, nonatomic) NSInteger rssi;
@property (assign, nonatomic) NSInteger txPower;
@property (assign, nonatomic) NSInteger batteryVoltage;
@property (assign, nonatomic) NSInteger temperature;
@property (assign, nonatomic) NSInteger humidity;
@property (assign, nonatomic) NSInteger gyroX;
@property (assign, nonatomic) NSInteger gyroY;
@property (assign, nonatomic) NSInteger gyroZ;
@property (assign, nonatomic) NSInteger isSend;

@end
