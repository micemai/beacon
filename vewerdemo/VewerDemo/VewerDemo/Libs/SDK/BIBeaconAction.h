//
//  BIBeaconAction.h
//  Beacon
//
//  Created by zhu on 2014/08/15.
//  Copyright (c) 2014年 Myuon Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

// アクション情報
@interface BIBeaconAction : NSObject
@property (assign, nonatomic) long actionId;
@property (strong, nonatomic) NSString *actionName;
@property (strong, nonatomic) NSArray *params;
@end