//
//  NSString+Extra.h
//  BeaconConnect
//
//  Created by zhu on 2014/07/17.
//  Copyright (c) 2014年 zhu. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BIUtils : NSObject 

/*!
    @method isEmpty
 
    @discussion emptyかをチェックする
 */
+ (BOOL) isEmpty:(NSString *)string;

/*!
    @method isNilOrEmpty
 
    @discussion ni||emptyかをチェックする
  */
+ (BOOL) isNilOrEmpty:(NSString *)string;

/*!
    @method hexStringWithData
 
    @param data コンバット元のデータ
    @return コンバット先の文字列
    @discussion emptyかをチェックする
 */
+ (NSString *) hexStringWithData:(NSData *)data;


+ (NSString *)UUIDStringFromCFUUIDRef:(CFUUIDRef)uuid;

@end
