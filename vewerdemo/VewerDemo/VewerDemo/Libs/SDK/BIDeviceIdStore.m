//
//  DeviceIdStore.m
//  Beaconnect-ios-sdk
//
//  Created by zhu on 2014/07/19.
//
//

#import "BIDeviceIdStore.h"
#import "BIUtils.h"

#import "UICKeyChainStore/UICKeyChainStore.h"
#import <UIKit/UIDevice.h>

static NSString * const kDeviceIdKeyChainStoreService = @"jp.basis-inn";
static NSString * const kDeviceIdKeyChainStoreKey = @"jp.basis-inn.deviceId";


@interface BIDeviceIdStore ()

@property (strong, nonatomic) UICKeyChainStore *keyChainStore;

@end

@implementation BIDeviceIdStore {
    NSString *_deviceId;
}

+ (instancetype)shareInstance
{
	static BIDeviceIdStore* _instance = nil;

    static dispatch_once_t oneToken;
	dispatch_once(&oneToken, ^{
		_instance = [[BIDeviceIdStore alloc] init];
		_instance.keyChainStore  = [UICKeyChainStore
                                   keyChainStoreWithService:kDeviceIdKeyChainStoreService];
	});
	return _instance;
}

- (BOOL)exists
{
    return ![BIUtils isNilOrEmpty:self.deviceId];
}

- (NSString *)create
{
    _deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [_keyChainStore setString:_deviceId forKey:kDeviceIdKeyChainStoreKey];
	[_keyChainStore synchronize];
    
    return self.deviceId;
}

- (NSString *)deviceId
{
	_deviceId = [_keyChainStore stringForKey:kDeviceIdKeyChainStoreKey];
	return _deviceId;
}


@end
