//
//  NSString+Extra.m
//  BeaconConnect
//
//  Created by zhu on 2014/07/17.
//  Copyright (c) 2014年 zhu. All rights reserved.
//

#import "BIUtils.h"

@implementation BIUtils

+ (BOOL) isEmpty:(NSString *)string
{
    return ([string length] == 0);
}

+ (BOOL) isNilOrEmpty:(NSString *)string
{
    return (string == nil || [self isEmpty:string]);
}

+ (NSString *) hexStringWithData:(NSData *)data
{
    NSUInteger stringLength = [data length] * 2;
    NSMutableString *stringBuffer = [NSMutableString stringWithCapacity:stringLength];
    const unsigned char *dataBuffer = [data bytes];
    NSInteger i;
    for (i=0; i< [data length]; ++i) {
        [stringBuffer appendFormat:@"%02x", (unsigned char)dataBuffer[i]];
    }
    return stringBuffer;
}

+ (NSString *)UUIDStringFromCFUUIDRef:(CFUUIDRef)uuid {
	CFStringRef string = CFUUIDCreateString(NULL, uuid);
	return CFBridgingRelease(string);
}

@end
