//
//  BIBeaconDetail.m
//  Beaconnect
//
//  Created by zhu on 2014/07/24.
//  Copyright (c) 2014年 zhu. All rights reserved.
//

#import "BIBeaconDetail.h"

@implementation BIBeaconDetail

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.action = [[BIBeaconAction alloc] init];
        self.placement = [[BIBeaconPlacement alloc] init];
        self.location = [[BIBeaconLocation alloc] init];
    }
    return self;
}
@end

@implementation BIBeaconPlacement
@end

@implementation BIBeaconLocation
@end