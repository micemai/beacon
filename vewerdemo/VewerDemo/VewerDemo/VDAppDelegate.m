//
//  VDAppDelegate.m
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/25/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import "VDAppDelegate.h"
#import "VDBeaconViewController.h"
#import "VDBeanconInfo.h"
@interface VDAppDelegate ()
@property (strong, nonatomic) BIBeaconnectClient *beaconnectClient;
@property (assign, nonatomic) BOOL doneScan;
@property (strong, nonatomic) VDBeaconViewController *beaconViewController;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@end
@implementation VDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    self.beaconViewController = [[VDBeaconViewController alloc] initWithNibName:@"VDBeaconViewController" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.beaconViewController];
    [self.window setRootViewController:navController];
    
    // Init Beaconnect client
    self.beaconnectClient = [[BIBeaconnectClient alloc] initWithBaseURL:[NSURL URLWithString:VDConstants_BeaconnectBaseURL] serviceId:VDConstants_SID apiKey:VDConstants_APIKey timeout:VDConstants_BeaconTimeoutInterval delegate:self];
    // Start Scanning
    [self scan];

    //init indicator view
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicatorView.backgroundColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.5];
    self.activityIndicatorView.frame = CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height);
    self.activityIndicatorView.center = CGPointMake(self.window.frame.size.width/2, self.window.frame.size.height/2);
	[self.activityIndicatorView startAnimating];
    [self.window makeKeyAndVisible];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
///////////////////////////////////////////////////////////////////////////////
#pragma mark -
////////////////////////////////////////////////////////////////////////////////

+ (instancetype)sharedAppDelegate
{
    return (VDAppDelegate *) [[UIApplication sharedApplication] delegate];
}

- (void)setDoneScan:(BOOL)doneScan{
    _doneScan = doneScan;
    
    if (!doneScan) {
        [self scan];
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [[UIApplication sharedApplication] cancelLocalNotification:notification];
	NSDictionary *userInfo = notification.userInfo;
	[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadName:userInfo[VDConstants_LocalNotificationNameKey] object:self userInfo:userInfo[VDConstants_LocalNotificationUserInfoKey]];
}

#pragma mark - BIBeaconnectScanningDelegate

/*
 * 検知ビーコンリストに新たに検知したビーコンが追加されたとき
 *
 * @param beacon 検知したビーコン
 */
- (void)client:(BIBeaconnectClient *)client didAddDetectList:(BIBeacon *)beacon
{
	NSLog(@"ビーコンを検出しました。");
	//	[self handleBeaconStateChanging:beacon];
}

/**
 * もっとも近くにあるビーコンが別のビーコンに変わったとき
 *
 * @param beacon 近くにあるビーコン
 */
- (void)client:(BIBeaconnectClient *)client didUpdateNearest:(BIBeacon *)beacon
{
	NSLog(@"一番近いビーコンが変わりました。");
	[self handleBeaconStateChanging:beacon];
}

/**
 * ビーコン範囲から出たとき（一定時間検知していないビーコンがあった場合）
 *
 * @param beacon 範囲から出たビーコン
 */
- (void)client:(BIBeaconnectClient *)client didOutOfRange:(BIBeacon *)beacon
{
	NSLog(@"ビーコンが見当たらなくなりました。beacon.UUID=%@", [beacon.uuid UUIDString]);
}

- (void)scan {
    
    [self.beaconnectClient startScan:self timeout:VDConstants_BeaconTimeoutInterval success:^{
        NSLog(@"スキャンの開始に成功しました。");
        self.doneScan = YES;
    } failure:^(NSError *error) {
        self.doneScan = NO;
        NSLog(@"スキャンの開始に失敗しました。");
    }];
    
}
+ (oneway void)retrieveBeaconDetailInClient:(BIBeaconnectClient *)client withBeacon:(BIBeacon *)beacon finish:(RetrieveBeaconDetailFinishBlock)block
{
	[client beaconDetail:beacon success:^(BIBeaconDetail *beaconDetail) {
		block(client, beaconDetail, beacon);
	} failure:^(NSError *error) {
		NSLog(@"ビーコン詳細の取得に失敗しました。%@", error);
	}];
}
- (void)handleBeaconStateChanging:(BIBeacon *)beacon
{
    [VDAppDelegate retrieveBeaconDetailInClient:self.beaconnectClient withBeacon:beacon finish:^(BIBeaconnectClient *client, BIBeaconDetail *beaconDetail, BIBeacon *beacon) {
        //display info
        if (self.beaconViewController) {
            VDBeanconInfo *beaconInfo = [[VDBeanconInfo alloc] init];
            beaconInfo.strUUID = [NSString stringWithFormat:@"UUID: %@",[beacon.uuid UUIDString]];
            beaconInfo.strMajor = [NSString stringWithFormat:@"Major: %@",beacon.major];
            beaconInfo.strMinor = [NSString stringWithFormat:@"Minor: %@",beacon.minor];
            beaconInfo.strRSSI = [NSString stringWithFormat:@"RSSI: %ld",(long)beacon.rssi];
            beaconInfo.strDateTime = [NSString stringWithFormat:@"検出日時: %@",beacon.detectionDate];
            beaconInfo.strBID = [NSString stringWithFormat:@"BID: %ld",(long)beaconDetail.bid];
            beaconInfo.strLocation = [NSString stringWithFormat:@"Location: %@",beaconDetail.location.locationName];
            beaconInfo.strPlacement = [NSString stringWithFormat:@"Placement: %@",beaconDetail.placement.placementName];
            if (beaconDetail.action.actionName ) {
                beaconInfo.strAction = [NSString stringWithFormat:@"Action: %@",beaconDetail.action.actionName];
            }
            else
            {
                beaconInfo.strAction = [NSString stringWithFormat:@"Action: %@",[beaconDetail.action.params objectAtIndex:0]];
            }
            beaconInfo.paramsAction = beaconDetail.action.params;
            BOOL bNewBeaconInfo = YES;
            if ([self.beaconViewController.arrBeaconInfo count] >0) {
                for (int i = 0; i<[self.beaconViewController.arrBeaconInfo count]; i++) {
                    VDBeanconInfo *beaconInfoCell = [self.beaconViewController.arrBeaconInfo objectAtIndex:i];
                    if ([beaconInfo.strUUID isEqualToString:beaconInfoCell.strUUID] && [beaconInfo.strMajor isEqualToString:beaconInfoCell.strMajor] && [beaconInfo.strMinor isEqualToString:beaconInfoCell.strMinor]) {
                        bNewBeaconInfo = NO;
                    }
                }
                if (bNewBeaconInfo) {
                    [self.beaconViewController.arrBeaconInfo addObject:beaconInfo];
                    [[NSNotificationCenter defaultCenter] postNotificationName:VDConstants_ReloadTableNotification object:self userInfo:nil];
                }
            }
            else
            {
                [self.beaconViewController.arrBeaconInfo addObject:beaconInfo];
                [[NSNotificationCenter defaultCenter] postNotificationName:VDConstants_ReloadTableNotification object:self userInfo:nil];
            }
        }
        
        
        
	}];

//	BCService *service = [BCService uuidIs:beacon.uuid];
//	if (service == nil) {
//		NSLog(@"検出したビーコンに該当するサービスはありません。 %@", beacon.uuid);
//		return;
//	}
//	
//	// detailを取得する。
//	__weak VDAppDelegate *weakSelf = self;
//	[service.serviceClient beaconDetail:beacon success:^(BIBeaconDetail *beaconDetail) {
//		NSLog(@"ビーコン詳細の取得に成功しました。");
//		// ログを保存する。
//        
//		// 通知する。
//		if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
//			// フォアグラウンドの場合は、画面に通知
//			[weakSelf showNotificationWithBeacon:beacon detail:beaconDetail];
//		} else {
//			// バックグラウンドの場合は、ローカル通知
//			[weakSelf sendNotificationWithBeacon:beacon detail:beaconDetail];
//		}
//	} failure:^(NSError *e) {
//		NSLog(@"ビーコン詳細の取得に失敗しました。 %@", e);
//	}];
}


- (void)showNotificationWithBeacon:(BIBeacon *)beacon detail:(BIBeaconDetail *)detail
{
	void (^proc)() = ^{
		NSArray *params = detail.action.params;
		if ([params count] < 1) {
			return;
		}
		
		NSString *title = @"";
		NSString *message = @"";
		NSString *contentURL = @"";
		
		// タイトル
		if (detail.placement.placementName) {
			title = detail.placement.placementName;
		} else if (detail.location.locationName) {
			title = detail.location.locationName;
		}
		
		// param2 は message
		if (params.count > 1) {
			message = params[1];
		}
		
		// param3 は 表示するコンテンツのURL
		if (params.count > 2) {
			contentURL = params[2];
		}
        //
        //		[TSMessage showNotificationInViewController:[BCAppDelegate sharedAppDelegate].window.rootViewController
        //											  title:title
        //										   subtitle:message
        //											  image:nil
        //											   type:TSMessageNotificationTypeMessage
        //										   duration:TSMessageNotificationDurationAutomatic
        //										   callback:nil
        //										buttonTitle:@"開く"
        //									 buttonCallback:^{
        //										 [self showWebViewWithURL:[NSURL URLWithString:contentURL]];
        //                                         NSLog(@"contentURL %@",contentURL);
        //									 }
        //										 atPosition:TSMessageNotificationPositionBottom
        //							   canBeDismissedByUser:YES];
        //		AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
	};
	
	if ([NSThread isMainThread]) {
		proc();
	} else {
		dispatch_async(dispatch_get_main_queue(), proc);
	}
}

- (void)sendNotificationWithBeacon:(BIBeacon *)beacon detail:(BIBeaconDetail *)detail
{
	NSArray *params = detail.action.params;
    if ([params count] < 1) {
        return;
    }
    
    NSString *message = @"";
	NSString *contentURL = @"";
    
	// param2 は message
	if (params.count > 1) {
		message = params[1];
	}
    
	// param3 は 表示するコンテンツのURL
	if (params.count > 2) {
		contentURL = params[2];
	}
    
    NSDate *date = [NSDate date];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.alertBody = message;
	notification.userInfo = @{ @"contentURL" : contentURL };
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.fireDate = date;
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

#pragma mark - Private

/**
 * Beaconnectのセットアップを行う。
 */


/**
 * アプリケーションがフォアグラウンドか調べる。
 */
- (BOOL)isForeground
{
	return [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive;
}
- (void)showIndicatorView:(BOOL)show
{
    if(show == TRUE) {
        [self.window addSubview:self.activityIndicatorView];
        [self.activityIndicatorView startAnimating];
    }
    else {
        [self.activityIndicatorView stopAnimating];
        [self.activityIndicatorView removeFromSuperview];
        
    }
}
@end
