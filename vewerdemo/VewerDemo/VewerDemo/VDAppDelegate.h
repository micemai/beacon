//
//  VDAppDelegate.h
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/25/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^RetrieveBeaconDetailFinishBlock)(BIBeaconnectClient *client, BIBeaconDetail *beaconDetail, BIBeacon *beacon);
@interface VDAppDelegate : UIResponder <UIApplicationDelegate, BIBeaconnectScanningDelegate>

@property (strong, nonatomic) UIWindow *window;
+ (instancetype)sharedAppDelegate;
- (void)showIndicatorView:(BOOL) show;
+ (oneway void)retrieveBeaconDetailInClient:(BIBeaconnectClient *)client withBeacon:(BIBeacon *)beacon finish:(RetrieveBeaconDetailFinishBlock)block;
@end
