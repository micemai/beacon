//
//  main.m
//  VewerDemo
//
//  Created by Nguyen Phuong Mai on 9/25/14.
//  Copyright (c) 2014 rikkeisoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VDAppDelegate class]));
    }
}
